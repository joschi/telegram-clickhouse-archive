drop table if exists default.test_array_joins
;
create table default.test_array_joins
(
    id UInt64,
    arr_1 Array(String),
    arr_2 Array(String),
    arr_3 Array(String),
    arr_4 Array(String)
) engine = MergeTree
order by tuple()
;
 alter table  default.test_array_joins modify column id UInt64 materialized getMaxIdTable('default','test_array_joins') + rowNumberInAllBlocks() + 1
;
insert into default.test_array_joins (arr_1, arr_2, arr_3, arr_4)
select array(randomPrintableASCII(1)),array(randomPrintableASCII(2)),array(randomPrintableASCII(3)),array(randomPrintableASCII(4))
from numbers(2)

;
select id,* from default.test_array_joins
order by id desc limit 100
;
-- drop function getMaxIdTable
-- ;
-- create function getMaxIdTable as (db,tabname) -> (select max(toUInt64OrZero(columns('^id$')::String)) from merge(db,tabname))
-- ;
-- select getMaxIdTable('default','test_array_joins')