try (ClickHouseConnection conn = dataSource.getConnection();
             ClickHouseStatement stmt = conn.createStatement()
) {
	stmt.write()
		.send("INSERT INTO test1 (" +
						"field_long, " +
						"field_string_nullable_1, " +
						"field_uuid_id_1, " +
						"field_string_2," +
						"field_string_3, " +
						"field_string_4, " +
						"field_big_decimal_128, " +
						"field_string_5, " +
						"last_updated_at, " +
						"field_uuid_id_2 " +
						")",
				stream -> {
					for (MyObject d : data) {
						stream.writeUInt64(d.getFieldLong());
						writeNullableString(stream, d.getFieldStringNullable());
						stream.writeUUID(d.getFieldUuidId1());
						stream.writeString(d.getFieldString2());
						stream.writeString(d.getFieldString3());
						stream.writeString(d.getFieldString4());
						stream.writeDecimal128(d.getTransactionAmount(), 7);
						stream.writeString(d.getFieldString5());
						stream.writeDateTime(d.getLastUpdatedAt());
						stream.writeUUID(d.getFieldUuidId2());
					}
				},
				ClickHouseFormat.RowBinary
		);
}

protected void writeNullableString(ClickHouseRowBinaryStream stream, String value) throws IOException {
        if (value == null) {
            stream.markNextNullable(true);
        } else {
            stream.markNextNullable(false);
            stream.writeString(value);
        }
    }