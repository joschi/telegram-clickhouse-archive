create table if not exists Audit.AuditData
(
    Number Int64,
    ClientName LowCardinality(String),
    ServiceName LowCardinality(String),
    OperationName LowCardinality(String),
    SenderName String,
    SenderRegister String,
    ReceiverName String,
    ReceiverRegister String null,
    IdSender String,
    IdReceiver String,
    DateTime DateTime64(6),
    Amount Decimal(30,18),
    PaymentSystem LowCardinality(String),
    CurrencyName LowCardinality(String),
    BalanceSender Nullable(Decimal(30,18)),
    BalanceReceiver Nullable(Decimal(30,18)),
    UpdateDateTime DateTime64(6) default now64(6)
) engine = ReplacingMergeTree(UpdateDateTime)
partition by (ClientName, toMonth(DateTime))
order by (ClientName, ServiceName, Number);