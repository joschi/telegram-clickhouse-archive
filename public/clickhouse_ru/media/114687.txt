create table `table`
(
    created          DateTime default now(),
    id               UInt64,
    field_1             String,
    field_2            UInt32,
    field_3         UInt32,
    field_4             Nullable(String),
    field_5       UInt64,
    field_6            UInt32,
    field_7 String,
    field_8              String,
    field_9            UInt8,
    field_10        Nullable(DateTime),
    field_11        Nullable(String),
    field_12          Nullable(String)
)
    engine = ReplacingMergeTree() ORDER BY intHash64(id) SETTINGS index_granularity = 8192;