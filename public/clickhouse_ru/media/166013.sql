CREATE TABLE IF NOT EXISTS purchases
(
    receipt_id String,
    app_id String,
    product String,
    product_group String,
    currency Nullable(String),
    customer_total_spent Int32,
    customer_payments_count Int32,
    customer_anon UInt8,
    customer_id String,
    customer_last_seen_at Nullable(DateTime64),
    start_app_version Nullable(String),
    app_version Nullable(String),
    sdk_version Nullable(String),
    os_version Nullable(String),
    device_type Nullable(String),
    country_iso_code Nullable(String),
    country Nullable(String),
    city Nullable(String),
    cohort Nullable(String),
    adjust_network Nullable(String),
    adjust_campaign Nullable(String),
    adjust_ad_group Nullable(String),
    adjust_creative Nullable(String),
    search_ads_campaign Nullable(String),
    search_ads_ad_group Nullable(String),
    search_ads_creative_set Nullable(String),
    search_ads_keyword Nullable(String),
    branch_partner Nullable(String),
    branch_secondary_publisher Nullable(String),
    branch_channel Nullable(String),
    branch_campaign Nullable(String),
    branch_adset Nullable(String),
    branch_adgroup Nullable(String),
    appsflyer_media_source Nullable(String),
    appsflyer_channel Nullable(String),
    appsflyer_campaign Nullable(String),
    appsflyer_adset Nullable(String),
    appsflyer_adgroup Nullable(String),
    cancellation_reason Nullable(String),
    is_intro_period UInt8,
    promo_offer_id Nullable(String),
    quantity Nullable(Int32),
    rule_id Nullable(String),
    screen_id Nullable(String),
    subscription_id String,
    transaction_id String,
    web_order_line_item_id Nullable(String),
    environment Int32,
    price_cent Int32,
    local_price_cent Int32,
    apple_comission Float32,
    unit Int32,
    units_count Int32,
    created_at DateTime64,
    purchased_at Nullable(DateTime64),
    cancelled_at Nullable(DateTime64),
    expires_at Nullable(DateTime64),
    customer_created_at DateTime64,
    kind Int32,
    is_trial_period UInt8,
    is_upgraded UInt8,
    _row_created_at Int64,
    _creating_reason String default 'default'
)
ENGINE = MergeTree()
PARTITION BY toYYYYMM(created_at)
ORDER BY (app_id, created_at, receipt_id)
