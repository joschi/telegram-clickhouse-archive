import clickhouse_connect
import configparser
from deep_translator import GoogleTranslator
from collections import Counter


# сбор данных о городах, городах-милионниках и каналах из txt-файлов
class Data():
    cities = {}
    millin_cities = {}
    channels = []

    def __init__(self):
        with open('million_cities.txt', encoding='utf8', errors='ignore') as mil:
            for line in mil.readlines():
                line = line.capitalize().rstrip('\n ')
                self.millin_cities[GoogleTranslator().translate(line)] = line
        with open('cities.txt', encoding='utf8', errors='ignore') as cit:
            for line in cit.readlines():
                line = line.capitalize().rstrip('\n ')
                self.cities[GoogleTranslator().translate(line)] = line
        with open('channels.txt', encoding='utf8', errors='ignore') as chan:
            for line in chan.readlines():
                line = line.capitalize().rstrip('\n ')
                self.channels.append(line)
        self.all_cities = self.millin_cities | self.cities


# функция сложения значений и объединения собранных из таблиц данных
def concatenate_dicts(dict1, dict2):
    for key in dict1.keys():        # складываем одинаковые значения из 2 словарей
        if key in dict2.keys():
            dict1[key] += dict2[key]
    dict1 = dict2 | dict1           # добавляем в первый словарь значения,  которых в нём нет
    return dict1


# убираем из словаря данные, которые не входят в список val_del и равномерно распределяем их значения по данным, входящим в val_insert
def filter_values(filter_dict, val_del, val_insert):
    count = 0                               # количество значений "мусорных" элементов
    for key in list(filter_dict):           # перебираем все элементы, удаляем мусор
        if key not in val_del:
            count += filter_dict[key]
            del filter_dict[key]
    count = int(count / len(val_insert))    # для равномерного распределния находим, какое значение прибавить к каждому элементу
    for key in filter_dict.keys():          # распределние значений этих городов по миллионникам
        if key in val_insert:
            filter_dict[key] += count
    return filter_dict


def main():
    info = Data()

    c = configparser.ConfigParser()
    c.read('config.ini')

    client_1 = clickhouse_connect.get_client(host=c['Server 1']['host'],
                                           port=int(c['Server 1']['port']),
                                           username=c['Server 1']['username'],
                                           password=c['Server 1']['password'])
    client_2 = clickhouse_connect.get_client(host=c['Server 2']['host'],
                                           port=int(c['Server 2']['port']),
                                           username=c['Server 2']['username'],
                                           password=c['Server 2']['password'])

    # ПО ГОРОДАМ:

    city_count = dict(client_1.query('''SELECT city, COUNT(city) FROM
                                    (SELECT DISTINCT terminal_id, city FROM directad.l
                                    WHERE t_stamp > DATE_SUB(HOUR, 1, NOW())
                                    ORDER BY terminal_id LIMIT 0,1000000)
                                   GROUP BY city''').result_set)
    city_2_en = client_2.query('''SELECT city, COUNT(city) FROM
                                    (SELECT DISTINCT terminal_uid, city FROM adstream.linear
                                    WHERE timestamp > DATE_SUB(HOUR, 1, NOW())
                                    ORDER BY terminal_uid LIMIT 0,1000000)
                                   GROUP BY city''').result_set

    city_2 = dict([(GoogleTranslator().translate(i[0]), i[1]) for i in city_2_en])
    city_count = concatenate_dicts(city_count, city_2)
    city_count = filter_values(city_count, info.all_cities.values(), info.millin_cities.values())
    print(city_count)

    # ПО КАНАЛАМ:

    channels_count = dict(client_1.query('''SELECT content, COUNT(content) FROM
                                    (SELECT DISTINCT terminal_id, content FROM directad.l
                                    WHERE t_stamp > DATE_SUB(HOUR, 1, NOW())
                                    ORDER BY terminal_id LIMIT 0,1000000)
                                   GROUP BY content''').result_set)
    channels_count_2 = dict(client_2.query('''SELECT channel, COUNT(channel) FROM
                                    (SELECT DISTINCT terminal_uid, channel FROM adstream.linear
                                    WHERE timestamp > DATE_SUB(HOUR, 1, NOW())
                                    ORDER BY terminal_uid LIMIT 0,1000000)
                                   GROUP BY channel''').result_set)

    channels_count = concatenate_dicts(channels_count, channels_count_2)
    channels_count = filter_values(channels_count, info.channels, info.channels)
    print(channels_count)

    # ПО ГОРОДАМ И КАНАЛАМ:

    city_and_channel_count = dict(client_1.query('''SELECT (city, content), COUNT(con) FROM
                                    (SELECT DISTINCT terminal_id, city, content, CONCAT(city, content) as con FROM directad.l
                                    WHERE t_stamp > DATE_SUB(HOUR, 1, NOW())
                                    ORDER BY terminal_id LIMIT 0,1000000)
                                   GROUP BY city, content''').result_set)
    city_and_channel_count_2 = dict(client_2.query('''select (city, channel), COUNT(cit) from
                                                    (select distinct terminal_uid, city, channel, concat(city, channel) as cit
                                                    from adstream.linear where timestamp > date_sub(hour,1,NOW()) order by terminal_uid limit 1000000)
                                                    group by city, channel''').result_set)
    city_and_channel_count = concatenate_dicts(city_and_channel_count, city_and_channel_count_2)
    # фильтрация. можно было бы написать красивее:
    count = 0
    for key in list(city_and_channel_count):
        if key[0].capitalize() not in info.channels or key[1] not in info.all_cities.values():       # если нет такого города или канала
            count += city_and_channel_count[key]
            del city_and_channel_count[key]
    count = int(count / len(info.millin_cities.values()))
    for key in city_and_channel_count.keys():
        if key[1] in info.millin_cities.values():
            city_and_channel_count[key] += count

    print(city_and_channel_count)


    client_1.command(f'INSERT INTO dsp.data_region(region, uniq) VALUES {tuple(city_count.items())}')
    
    client_1.command(f'INSERT INTO dsp.data_channel(channel, uniq) VALUES {tuple(channels_count.values())}')

    for i in city_and_channel_count.keys():
        print(i)
        client_1.command(f'INSERT INTO dsp.data_channel_region(channel, region, uniq) VALUES ({repr(i[1])}, {repr(i[0])}, {city_and_channel_count[i]})')


if __name__ == '__main__':
    main()
