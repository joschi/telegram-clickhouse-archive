with t0 as 
(
    select *
    from test.current_partition
    where block_timestamp > '2022-01-02'
),
t1 as 
(
    select 
        arrayJoin(`outputs.addresses`) as output_address, 
        hash, 
        `block_timestamp`,
        ROW_NUMBER() OVER (PARTITION BY output_address ORDER BY `block_timestamp`) as num
    from t0
), 
t_sub2 as 
(
    select *, if(num=1, 0, 1) as existed_in_sample, /*output_address in (
        select change_address
        from test.OTC_hashes_copy_merge_tree as t1
        ) as existed_in_otc_changes,*/
        if(t1.`block_timestamp` = afa.`timestamp`, 0, 1) as existed_ever_before,
        ROW_NUMBER() OVER (PARTITION BY output_address, existed_ever_before ORDER BY `block_timestamp`) as num2
        from test.address_first_appearance as afa
        right join t1
        on afa.address = t1.output_address[1]
),
t21 as 
(
	select *,
		t2.hash as hash, 
		t2.block_timestamp as block_timestamp,
		t2.existed_in_sample as existed_in_sample,
		t2.existed_ever_before as existed_ever_before,
		t2.address as address, 
		(t1.change_address is NULL) as existed_in_otc_changes
	from test.OTC_hashes_copy_merge_tree as t1
	right join t_sub2 as t2
	on t2.hash = t1.hash
	where existed_in_otc_changes::Bool
)
select * from t21
where not existed_in_otc_changes::Bool
