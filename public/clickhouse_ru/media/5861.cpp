#include <string>
#include <fstream>
#include <iostream>

int main(int argc, char* argv[])
{
    if (argc < 3) {
        std::cout << "usage: " << argv[0] << " <count> <path/to/file>" << std::endl;
        return 1;
    }

    std::string::size_type sz;   // alias of size_t
    const int count = std::stoi(argv[1], &sz);

    const std::string path(argv[2]);
    std::ofstream output(path);

//    output << "INSERT INTO test (date, str1, str2, str3) VALUES";
    size_t i = 17700;
    for(; i < 17700 + count - 1; i++) {
        output << "(" << i << ", " << "'str1'" << ", " << "'str2'" << ", " << "'str" << i << "'" << "), ";
    }
    output << "(" << i << ", " << "'str1'" << ", " << "'str2'" << ", " << "'str" << i << "'" << ")" << std::endl;

    return 0;
}
