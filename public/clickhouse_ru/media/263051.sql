WITH
    EVENTS AS (
        SELECT
            event_name,
            user_pseudo_id,
            event_timestamp,
            event_params.key,
            event_params.string_value,
            event_params.float_value,
            divide(event_params.float_value[indexOf(event_params.key, 'valuemicros')], 1000000) AS valuemicros
        FROM app1_smart_security.events
        WHERE
            event_date_date BETWEEN DATE('2022-03-01') AND DATE('2022-03-28')
            AND DATE(fromUnixTimestamp64Milli(intDiv(user_first_touch_timestamp, 1000))) BETWEEN DATE('2022-03-01') AND DATE('2022-03-28')
            AND user_pseudo_id IS NOT NULL
            AND event_name IN (
                'S_af_conversion_data',
                'paid_ad_impression',
                'S_ad_impression',
                'S_ad_click'
            )
    )
    ,
    REALTIME_EVENTS AS (
        SELECT
            event_name,
            user_pseudo_id,
            event_timestamp,
            event_params.key,
            event_params.string_value,
            event_params.float_value,
            divide(event_params.float_value[indexOf(event_params.key, 'valuemicros')], 1000000) AS valuemicros
        FROM app1_smart_security.events_intraday
        WHERE
            event_date_date BETWEEN DATE('2022-03-01') AND DATE('2022-03-28')
            AND DATE(fromUnixTimestamp64Milli(intDiv(user_first_touch_timestamp, 1000))) BETWEEN DATE('2022-03-01') AND DATE('2022-03-28')
            AND user_pseudo_id IS NOT NULL
            AND event_name IN (
                'S_af_conversion_data',
                'paid_ad_impression',
                'S_ad_impression',
                'S_ad_click'
            )
            AND event_timestamp > (SELECT MAX(event_timestamp) FROM EVENTS)
    )
    ,
    UNION_EVENTS AS (
        SELECT * FROM EVENTS
        UNION ALL
        SELECT * FROM REALTIME_EVENTS
    )
    ,
    WITH_ANALUTIC_FUNCTION_DATA AS (
        SELECT
            *,
            COUNT(CASE WHEN event_name = 'S_ad_impression' THEN 1 END) OVER (PARTITION BY user_pseudo_id ORDER BY user_pseudo_id) AS impressions,
            COUNT(CASE WHEN event_name = 'S_ad_click' THEN 1 END) OVER (PARTITION BY user_pseudo_id ORDER BY user_pseudo_id) AS clicks,
            SUM(valuemicros) OVER (PARTITION BY user_pseudo_id ORDER BY user_pseudo_id) AS revenue
        FROM UNION_EVENTS
    )

SELECT * FROM WITH_ANALUTIC_FUNCTION_DATA LIMIT 0, 100