MacBook-Pro-Mark:build Mark$ cmake .. -DCMAKE_CXX_COMPILER=`which g++-6` -DCMAKE_C_COMPILER=`which gcc-6`
CMake Deprecation Warning at CMakeLists.txt:27 (cmake_policy):
  The OLD behavior for policy CMP0014 will be removed from a future version
  of CMake.

  The cmake-policies(7) manual explains that the OLD behaviors of all
  policies are deprecated and that a policy should be set to OLD only under
  specific short-term circumstances.  Projects should be ported to the NEW
  behavior and not rely on setting a policy to OLD.


-- CMAKE_BUILD_TYPE is not set, set to default = RELWITHDEBINFO
-- CMAKE_BUILD_TYPE: RELWITHDEBINFO
-- Tests are enabled
-- Building for: Darwin-16.7.0 x86_64  ; USE_STATIC_LIBRARIES=0 MAKE_STATIC_LIBRARIES=ON UNBUNDLED=OFF CCACHE=CCACHE_FOUND-NOTFOUND 
-- Using openssl=TRUE: /usr/local/opt/openssl/include : /usr/local/opt/openssl/lib/libssl.a;/usr/local/opt/openssl/lib/libcrypto.a
-- Using icu=1: /usr/local/opt/icu4c/include : /usr/local/opt/icu4c/lib/libicui18n.a;/usr/local/opt/icu4c/lib/libicuuc.a;/usr/local/opt/icu4c/lib/libicudata.a
-- Using Boost: /Users/Mark/ClickHouse/contrib/libboost/boost_1_65_0/ : boost_program_options_internal,boost_system_internal,boost_filesystem_internal
-- Using zlib: /Users/Mark/ClickHouse/contrib/libzlib-ng : zlib
-- Using zstd:  : zstd
-- Using ltdl: LTDL_LIB-NOTFOUND
-- Using Poco: /Users/Mark/ClickHouse/contrib/libpoco/Foundation/include/;/Users/Mark/ClickHouse/contrib/libpoco/Util/include/;/Users/Mark/ClickHouse/contrib/libpoco/Net/include/;/Users/Mark/ClickHouse/contrib/libpoco/Data/include/;/Users/Mark/ClickHouse/contrib/libpoco/XML/include/;/Users/Mark/ClickHouse/contrib/libpoco/MongoDB/include/;/Users/Mark/ClickHouse/contrib/libpoco/Data/ODBC/include/;/Users/Mark/ClickHouse/contrib/libpoco/NetSSL_OpenSSL/include/;/Users/Mark/ClickHouse/contrib/libpoco/Crypto/include/ : PocoFoundation,PocoUtil,PocoNet,PocoNetSSL,PocoXML,PocoData,PocoDataODBC;LTDL_LIB-NOTFOUND,PocoMongoDB; MongoDB=1, DataODBC=1, NetSSL=1
-- Using lz4:  : lz4
-- Using sparsehash: /Users/Mark/ClickHouse/contrib/libsparsehash
-- Using rt: apple_rt
-- Using line editing libraries (readline): /usr/local/opt/readline/include : /usr/local/opt/readline/lib/libreadline.a;/usr/lib/libtermcap.dylib
-- Using zookeeper:  : zookeeper_mt
-- Using re2:  : re2;  : re2_st
-- Using cityhash: /Users/Mark/ClickHouse/contrib/libcityhash/include : cityhash
-- Using farmhash:  : farmhash
-- Using metrohash:  : metrohash
-- Using btrie:  : btrie
-- Using double-conversion: /Users/Mark/ClickHouse/contrib/libdouble-conversion : double-conversion
-- Using tcmalloc=1: /Users/Mark/ClickHouse/contrib/libtcmalloc/include : tcmalloc_minimal_internal
-- Using cctz: /Users/Mark/ClickHouse/contrib/libcctz/include : cctz
-- Using mysqlclient=1: /usr/local/include : /usr/local/lib/libmariadbclient.a; staticlib=/usr/local/lib/libmariadbclient.a
-- Using unwind=:  : 
-- C_FLAGS    =   -D_GLIBCXX_USE_CXX11_ABI=1 -pipe -msse4.1 -msse4.2 -mpopcnt -fno-omit-frame-pointer -Wall  -O2 -g -DNDEBUG -O3
-- CXX_FLAGS  =   -D_GLIBCXX_USE_CXX11_ABI=1 -pipe -msse4.1 -msse4.2 -mpopcnt -std=gnu++1z  -fno-omit-frame-pointer -Wall -Wnon-virtual-dtor  -O2 -g -DNDEBUG -O3
-- LINK_FLAGS = 
-- Poco package version: 1.6.1-all (2015-08-04)
-- Setting Poco build type - RELWITHDEBINFO
-- Building static libraries
-- Building without tests & samples
-- Build with using internal copy of sqlite, pcre, expat, ...
-- ODBC Support Enabled
-- Found APR: /usr/lib/libapr-1.dylib
-- Found APRUTIL: /usr/lib/libaprutil-1.dylib
-- Found APACHE: /usr/include/apache2
-- CMake 3.9.4 successfully configured Poco using Unix Makefiles generator
-- Installation target path: /usr/local
-- Building: XML
-- Building: MongoDB
-- Building: Util
-- Building: Net
-- Building: NetSSL_OpenSSL
-- Building: Crypto
-- Building: Data
-- ZSTD VERSION 1.3.17
-- Architecture: x86_64
-- Architecture-specific source files: arch/x86/x86.c;arch/x86/insert_string_sse.c;arch/x86/fill_window_sse.c;arch/x86/crc_folding.c
-- Building: tcmalloc_minimal_internal
-- Link libtcmalloc_minimal : tcmalloc_minimal_internal
-- Will build ClickHouse 1.1.54292
-- Using internal compiler: headers=/usr/share/clickhouse/headers : /usr/share/clickhouse/bin/clang   -D_GLIBCXX_USE_CXX11_ABI=1 -pipe -msse4.1 -msse4.2 -mpopcnt -std=gnu++1z  -fno-omit-frame-pointer -Wall -Wnon-virtual-dtor  -Werror -O3 -DNDEBUG -x c++ -march=native -shared -fPIC -fvisibility=hidden -fno-implement-inlines 
-- Target check already exists
CMake Error: The following variables are used in this project, but they are set to NOTFOUND.
Please set them or make sure they are set and tested correctly in the CMake files:
LTDL_LIB
    linked by target "dbms" in directory /Users/Mark/ClickHouse/dbms

-- Configuring incomplete, errors occurred!
See also "/Users/Mark/ClickHouse/build/CMakeFiles/CMakeOutput.log".
See also "/Users/Mark/ClickHouse/build/CMakeFiles/CMakeError.log".