CREATE DATABASE IF NOT EXISTS clickhouse;

DROP TABLE IF EXISTS clickhouse.kafka_ops;

CREATE TABLE IF NOT EXISTS clickhouse.kafka_ops
(
	account_number   String,   -- 40702810310000887003
	operation_type   String,   -- debit
	operation_status String,   -- transaction
	rrn              String    -- 123456789012    
) ENGINE = Kafka() 
SETTINGS
kafka_broker_list = 'kafka:9092',
kafka_topic_list = 'ops',
kafka_group_name = 'test',
kafka_format = 'test:Operation',
kafka_num_consumers = 4;
