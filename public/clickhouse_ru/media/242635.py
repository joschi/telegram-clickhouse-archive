import asyncio

from asynch.errors import TypeMismatchError
from asynch import create_pool


async def execute_query(connection_pool, query, values=None):
    async with connection_pool.acquire() as connection:
        async with connection.cursor() as cursor:
            if values is not None:
                await cursor.execute(query, values)
            else:
                await cursor.execute(query)


async def main():
    connection_pool = await create_pool(
        host='127.0.0.1',
        port='9000',
        database='default',
        user='default',
        password='',
        maxsize=30,
    )

    await execute_query(
        connection_pool=connection_pool,
        query="""
        CREATE TABLE IF NOT EXISTS test(data UInt32) ENGINE = MergeTree()
        ORDER BY(data) PRIMARY KEY (data) PARTITION BY (data)
        """,
    )

    invalid_values = [(429496729500,)] + [(x,) for x in range(100)]

    try:
        await execute_query(
            connection_pool=connection_pool,
            query="INSERT INTO test(data) VALUES",
            values=invalid_values,
        )
    except TypeMismatchError as exc:
        print(exc)
        del invalid_values[0]
        await execute_query(
            connection_pool=connection_pool,
            query="INSERT INTO test(data) VALUES",
            values=invalid_values,
        )


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    event_loop.create_task(main())
    event_loop.run_forever()
