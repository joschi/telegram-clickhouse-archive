drop table email_stats.email_events_aggregated_2;
CREATE TABLE IF NOT EXISTS email_stats.email_events_aggregated_2
(
    date                        Date,
    type                        String,
    brand_id                    String,
    campaign__id                String,
    audience__id                String,
    legacy_campaign__id         String,
    legacy_audience__id         UInt32,
    promo__id                   UInt32,
    location__id                String,

    unique_send                 AggregateFunction(uniq, String),
    unique_send_one_off         AggregateFunction(uniq, String),
    unique_send_test            AggregateFunction(uniq, String),
    unique_processed            AggregateFunction(uniq, Tuple(String, String)),
    unique_delivered            AggregateFunction(uniq, Tuple(String, String)),
    unique_deferred             AggregateFunction(uniq, Tuple(String, String)),
    unique_dropped              AggregateFunction(uniq, Tuple(String, String)),
    unique_bounced              AggregateFunction(uniq, Tuple(String, String)),
    unique_click                AggregateFunction(uniq, String),
    total_click                 AggregateFunction(uniq, Tuple(String, String)),
    unique_open                 AggregateFunction(uniq, String),
    total_open                  AggregateFunction(uniq, Tuple(String, String)),
    unique_spam_report          AggregateFunction(uniq, Tuple(String, String)),
    unique_unsubscribed         AggregateFunction(uniq, Tuple(String, String)),

    promo__codes_issued         AggregateFunction(count),
    promo__success_page_open    AggregateFunction(count),
    promo__invalid_page_open    AggregateFunction(count),

    promo__rejected_1d          AggregateFunction(count),
    promo__redeemed_1d          AggregateFunction(count),
    promo__redemption_sales_1d  AggregateFunction(sum, Float32),
    promo__cost_1d              AggregateFunction(sum, Float32),

    promo__rejected_7d          AggregateFunction(count),
    promo__redeemed_7d          AggregateFunction(count),
    promo__redemption_sales_7d  AggregateFunction(sum, Float32),
    promo__cost_7d              AggregateFunction(sum, Float32),

    promo__rejected_14d         AggregateFunction(count),
    promo__redeemed_14d         AggregateFunction(count),
    promo__redemption_sales_14d AggregateFunction(sum, Float32),
    promo__cost_14d             AggregateFunction(sum, Float32),

    promo__rejected_21d         AggregateFunction(count),
    promo__redeemed_21d         AggregateFunction(count),
    promo__redemption_sales_21d AggregateFunction(sum, Float32),
    promo__cost_21d             AggregateFunction(sum, Float32),

    promo__rejected_30d         AggregateFunction(count),
    promo__redeemed_30d         AggregateFunction(count),
    promo__redemption_sales_30d AggregateFunction(sum, Float32),
    promo__cost_30d             AggregateFunction(sum, Float32),

    txn__by_delivered_count_1d  AggregateFunction(count),
    txn__by_delivered_sales_1d  AggregateFunction(sum, Float32),
    txn__by_open_count_1d       AggregateFunction(count),
    txn__by_open_sales_1d       AggregateFunction(sum, Float32),
    txn__by_click_count_1d      AggregateFunction(count),
    txn__by_click_sales_1d      AggregateFunction(sum, Float32),

    txn__by_delivered_count_7d  AggregateFunction(count),
    txn__by_delivered_sales_7d  AggregateFunction(sum, Float32),
    txn__by_open_count_7d       AggregateFunction(count),
    txn__by_open_sales_7d       AggregateFunction(sum, Float32),
    txn__by_click_count_7d      AggregateFunction(count),
    txn__by_click_sales_7d      AggregateFunction(sum, Float32),

    txn__by_delivered_count_14d AggregateFunction(count),
    txn__by_delivered_sales_14d AggregateFunction(sum, Float32),
    txn__by_open_count_14d      AggregateFunction(count),
    txn__by_open_sales_14d      AggregateFunction(sum, Float32),
    txn__by_click_count_14d     AggregateFunction(count),
    txn__by_click_sales_14d     AggregateFunction(sum, Float32),

    txn__by_delivered_count_21d AggregateFunction(count),
    txn__by_delivered_sales_21d AggregateFunction(sum, Float32),
    txn__by_open_count_21d      AggregateFunction(count),
    txn__by_open_sales_21d      AggregateFunction(sum, Float32),
    txn__by_click_count_21d     AggregateFunction(count),
    txn__by_click_sales_21d     AggregateFunction(sum, Float32),

    txn__by_delivered_count_30d AggregateFunction(count),
    txn__by_delivered_sales_30d AggregateFunction(sum, Float32),
    txn__by_open_count_30d      AggregateFunction(count),
    txn__by_open_sales_30d      AggregateFunction(sum, Float32),
    txn__by_click_count_30d     AggregateFunction(count),
    txn__by_click_sales_30d     AggregateFunction(sum, Float32)

) ENGINE = ReplicatedAggregatingMergeTree('/clickhouse/{cluster}/tables/aggregated_email_events_2/{shard}', '{replica}')
      PARTITION BY date
      ORDER BY (date, type, brand_id, campaign__id, audience__id, legacy_campaign__id, legacy_audience__id, promo__id, location__id)
      SETTINGS index_granularity = 8192;


drop table email_stats.email_events_aggregated_view_2;

CREATE MATERIALIZED VIEW IF NOT EXISTS email_stats.email_events_aggregated_view_2
    TO email_stats.email_events_aggregated_2
AS

SELECT toDate(timestamp)                                                                                                                                            as date,
       type                                                                                                                                                         as type,
       brand_id                                                                                                                                                     as brand_id,
       if(campaign_id is not null, assumeNotNull(campaign_id), '')                                                                                                  as campaign__id,
       if(audience_id is not null, assumeNotNull(audience_id), '')                                                                                                  as audience__id,
       if(legacy_campaign_id is not null, assumeNotNull(legacy_campaign_id), '')                                                                                    as legacy_campaign__id,
       if(legacy_audience_group_id is not null, assumeNotNull(legacy_audience_group_id), toUInt32(0))                                                               as legacy_audience__id,
       if(promotion_id is not null, assumeNotNull(promotion_id), toUInt32(0))                                                                                       as promo__id,
       if(location_id is not null, assumeNotNull(location_id), '')                                                                                                  as location__id,

       uniqStateIf(bridg_message_id, type = 'SEND')                                                                                                                 as unique_send,
       uniqStateIf(bridg_message_id, type = 'SEND_ONE_OFF')                                                                                                         as unique_send_one_off,
       uniqStateIf(bridg_message_id, type = 'SEND_TEST')                                                                                                            as unique_send_test,
       uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'PROCESSED')                                            as unique_processed,
       uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'DELIVERED')                                            as unique_delivered,
       uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'DEFERRED')                                             as unique_deferred,
       uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'DROPPED')                                              as unique_dropped,
       uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'BOUNCE')                                               as unique_bounced,
       uniqStateIf(bridg_message_id, type = 'CLICK')                                                                                                                as unique_click,
       uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'CLICK')                                                as total_click,
       uniqStateIf(bridg_message_id, type = 'OPEN')                                                                                                                 as unique_open,
       uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'OPEN')                                                 as total_open,
       uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))),
                   type = 'SPAM_REPORT')                                                                                                                            as unique_spam_report,
       uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))),
                   type = 'UNSUBSCRIBE')                                                                                                                            as unique_unsubscribed,

       countStateIf(type = 'PROMO_CODE_RESERVED')                                                                                                                   as promo__codes_issued,
       countStateIf(type = 'PROMO_SUCCESS_PAGE_OPENED')                                                                                                             as promo__success_page_open,
       countStateIf(type = 'PROMO_INVALID_PAGE_OPENED')                                                                                                             as promo__invalid_page_open,

       countStateIf(type = 'PROMO_REWARD_REDEEMED' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 1))  as promo__redeemed_1d,
       countStateIf(type = 'PROMO_REWARD_REJECTED' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 1))  as promo__rejected_1d,
       sumStateIf(if(promo_redeem_sales is null, toFloat32(0), assumeNotNull(promo_redeem_sales)),
                  type = 'PROMO_REWARD_REDEEMED' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 1))    as promo__redemption_sales_1d,
       sumStateIf(if(promo_redeem_discount is null, toFloat32(0), assumeNotNull(promo_redeem_discount)), type = 'PROMO_REWARD_REDEEMED' and date < addDays(
               toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                         parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 1))                      as promo__cost_1d,


       countStateIf(type = 'PROMO_REWARD_REDEEMED' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 7))  as promo__redeemed_7d,
       countStateIf(type = 'PROMO_REWARD_REJECTED' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 7))  as promo__rejected_7d,
       sumStateIf(if(promo_redeem_sales is null, toFloat32(0), assumeNotNull(promo_redeem_sales)),
                  type = 'PROMO_REWARD_REDEEMED' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 7))    as promo__redemption_sales_7d,
       sumStateIf(if(promo_redeem_discount is null, toFloat32(0), assumeNotNull(promo_redeem_discount)), type = 'PROMO_REWARD_REDEEMED' and date < addDays(
               toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                         parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 7))                      as promo__cost_7d,


       countStateIf(type = 'PROMO_REWARD_REDEEMED' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 14)) as promo__redeemed_14d,
       countStateIf(type = 'PROMO_REWARD_REJECTED' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 14)) as promo__rejected_14d,
       sumStateIf(if(promo_redeem_sales is null, toFloat32(0), assumeNotNull(promo_redeem_sales)),
                  type = 'PROMO_REWARD_REDEEMED' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))),
                                 14))                                                                                                                               as promo__redemption_sales_14d,
       sumStateIf(if(promo_redeem_discount is null, toFloat32(0), assumeNotNull(promo_redeem_discount)),
                  type = 'PROMO_REWARD_REDEEMED' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 14))   as promo__cost_14d,


       countStateIf(type = 'PROMO_REWARD_REDEEMED' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 21)) as promo__redeemed_21d,
       countStateIf(type = 'PROMO_REWARD_REJECTED' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 21)) as promo__rejected_21d,
       sumStateIf(if(promo_redeem_sales is null, toFloat32(0), assumeNotNull(promo_redeem_sales)),
                  type = 'PROMO_REWARD_REDEEMED' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))),
                                 21))                                                                                                                               as promo__redemption_sales_21d,
       sumStateIf(if(promo_redeem_discount is null, toFloat32(0), assumeNotNull(promo_redeem_discount)),
                  type = 'PROMO_REWARD_REDEEMED' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 21))   as promo__cost_21d,


       countStateIf(type = 'PROMO_REWARD_REDEEMED' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 30)) as promo__redeemed_30d,
       countStateIf(type = 'PROMO_REWARD_REJECTED' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 30)) as promo__rejected_30d,
       sumStateIf(if(promo_redeem_sales is null, toFloat32(0), assumeNotNull(promo_redeem_sales)),
                  type = 'PROMO_REWARD_REDEEMED' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))),
                                 30))                                                                                                                               as promo__redemption_sales_30d,
       sumStateIf(if(promo_redeem_discount is null, toFloat32(0), assumeNotNull(promo_redeem_discount)),
                  type = 'PROMO_REWARD_REDEEMED' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 30))   as promo__cost_30d,


       countStateIf(
                   type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and
                   date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                            parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 1))   as txn__by_delivered_count_1d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 1))    as txn__by_delivered_sales_1d,
       countStateIf(type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 1))  as txn__by_open_count_1d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 1))    as txn__by_open_sales_1d,
       countStateIf(
                   type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and
                   date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                            parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 1))   as txn__by_click_count_1d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 1))    as txn__by_click_sales_1d,


       countStateIf(
                   type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and
                   date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                            parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 7))   as txn__by_delivered_count_7d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 7))    as txn__by_delivered_sales_7d,
       countStateIf(type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 7))  as txn__by_open_count_7d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 7))    as txn__by_open_sales_7d,
       countStateIf(
                   type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and
                   date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                            parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 7))   as txn__by_click_count_7d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 7))    as txn__by_click_sales_7d,


       countStateIf(
                   type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and
                   date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                            parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))),
                                  14))                                                                                                                              as txn__by_delivered_count_14d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))),
                                 14))                                                                                                                               as txn__by_delivered_sales_14d,
       countStateIf(type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 14)) as txn__by_open_count_14d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 14))   as txn__by_open_sales_14d,
       countStateIf(
                   type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and
                   date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                            parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 14))  as txn__by_click_count_14d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 14))   as txn__by_click_sales_14d,


       countStateIf(
                   type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and
                   date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                            parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))),
                                  21))                                                                                                                              as txn__by_delivered_count_21d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))),
                                 21))                                                                                                                               as txn__by_delivered_sales_21d,
       countStateIf(type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 21)) as txn__by_open_count_21d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 21))   as txn__by_open_sales_21d,
       countStateIf(
                   type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and
                   date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                            parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 21))  as txn__by_click_count_21d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 21))   as txn__by_click_sales_21d,


       countStateIf(
                   type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and
                   date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                            parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))),
                                  30))                                                                                                                              as txn__by_delivered_count_30d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))),
                                 30))                                                                                                                               as txn__by_delivered_sales_30d,
       countStateIf(type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and
                    date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                             parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 30)) as txn__by_open_count_30d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 30))   as txn__by_open_sales_30d,
       countStateIf(
                   type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and
                   date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                            parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 30))  as txn__by_click_count_30d,
       sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                  type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and
                  date < addDays(toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                                           parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id)))))), 30))   as txn__by_click_sales_30d

from email_stats.email_events

where ((campaign_id is not null and audience_id is not null) or (legacy_campaign_id is not null and legacy_audience_group_id is not null))
  and is_test = 0
  and is_send_failed = 0

group by date,
         type,
         brand_id,
         campaign_id,
         audience_id,
         legacy_campaign_id,
         legacy_audience_group_id,
         promotion_id,
         location_id;

drop table email_stats.email_events_aggregated_dist_2;
CREATE TABLE IF NOT EXISTS email_stats.email_events_aggregated_dist_2
    AS email_stats.email_events_aggregated_2
        ENGINE = Distributed(
                 '{cluster}',
                 'email_stats',
                 email_events_aggregated_2,
                 rand()
            );

insert into email_stats.email_events_aggregated_2
select date,
       type,
       brand_id,
       campaign__id,
       audience__id,
       legacy_campaign__id,
       legacy_audience__id,
       promo__id,
       location__id,

       unique_send,
       unique_send_one_off,
       unique_send_test,
       unique_processed,
       unique_delivered,
       unique_deferred,
       unique_dropped,
       unique_bounced,
       unique_click,
       total_click,
       unique_open,
       total_open,
       unique_spam_report,
       unique_unsubscribed,

       promo__codes_issued,
       promo__success_page_open,
       promo__invalid_page_open,

       promo__rejected_1d,
       promo__redeemed_1d,
       promo__redemption_sales_1d,
       promo__cost_1d,

       promo__rejected_7d,
       promo__redeemed_7d,
       promo__redemption_sales_7d,
       promo__cost_7d,

       promo__rejected_14d,
       promo__redeemed_14d,
       promo__redemption_sales_14d,
       promo__cost_14d,

       promo__rejected_21d,
       promo__redeemed_21d,
       promo__redemption_sales_21d,
       promo__cost_21d,

       promo__rejected_30d,
       promo__redeemed_30d,
       promo__redemption_sales_30d,
       promo__cost_30d,

       txn__by_delivered_count_1d,
       txn__by_delivered_sales_1d,
       txn__by_open_count_1d,
       txn__by_open_sales_1d,
       txn__by_click_count_1d,
       txn__by_click_sales_1d,

       txn__by_delivered_count_7d,
       txn__by_delivered_sales_7d,
       txn__by_open_count_7d,
       txn__by_open_sales_7d,
       txn__by_click_count_7d,
       txn__by_click_sales_7d,

       txn__by_delivered_count_14d,
       txn__by_delivered_sales_14d,
       txn__by_open_count_14d,
       txn__by_open_sales_14d,
       txn__by_click_count_14d,
       txn__by_click_sales_14d,

       txn__by_delivered_count_21d,
       txn__by_delivered_sales_21d,
       txn__by_open_count_21d,
       txn__by_open_sales_21d,
       txn__by_click_count_21d,
       txn__by_click_sales_21d,

       txn__by_delivered_count_30d,
       txn__by_delivered_sales_30d,
       txn__by_open_count_30d,
       txn__by_open_sales_30d,
       txn__by_click_count_30d,
       txn__by_click_sales_30d
FROM (
      SELECT toDate(timestamp)                                                                                                                                                       as date,
             type                                                                                                                                                                    as type,
             brand_id                                                                                                                                                                as brand_id,
             if(campaign_id is not null, assumeNotNull(campaign_id), '')                                                                                                             as campaign__id,
             if(audience_id is not null, assumeNotNull(audience_id), '')                                                                                                             as audience__id,
             if(legacy_campaign_id is not null, assumeNotNull(legacy_campaign_id), '')                                                                                               as legacy_campaign__id,
             if(legacy_audience_group_id is not null, assumeNotNull(legacy_audience_group_id), toUInt32(0))                                                                          as legacy_audience__id,
             if(promotion_id is not null, assumeNotNull(promotion_id), toUInt32(0))                                                                                                  as promo__id,
             if(location_id is not null, assumeNotNull(location_id), '')                                                                                                             as location__id,

             toDate(if(campaign__id != '', parseDateTimeBestEffort(dictGetString('email_blasts', 'start_time', tuple(upper(campaign__id)))),
                       parseDateTimeBestEffort(dictGetString('legacy_campaigns', 'delivery_time', tuple(upper(legacy_campaign__id))))))                                              AS scheduled_date,

             uniqStateIf(bridg_message_id, type = 'SEND')                                                                                                                            as unique_send,
             uniqStateIf(bridg_message_id, type = 'SEND_ONE_OFF')                                                                                                                    as unique_send_one_off,
             uniqStateIf(bridg_message_id, type = 'SEND_TEST')                                                                                                                       as unique_send_test,
             uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'PROCESSED')                                                       as unique_processed,
             uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'DELIVERED')                                                       as unique_delivered,
             uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'DEFERRED')                                                        as unique_deferred,
             uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'DROPPED')                                                         as unique_dropped,
             uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'BOUNCE')                                                          as unique_bounced,
             uniqStateIf(bridg_message_id, type = 'CLICK')                                                                                                                           as unique_click,
             uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'CLICK')                                                           as total_click,
             uniqStateIf(bridg_message_id, type = 'OPEN')                                                                                                                            as unique_open,
             uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))), type = 'OPEN')                                                            as total_open,
             uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))),
                         type = 'SPAM_REPORT')                                                                                                                                       as unique_spam_report,
             uniqStateIf(tuple(bridg_message_id, if(sg_event_id is null, '', assumeNotNull(sg_event_id))),
                         type = 'UNSUBSCRIBE')                                                                                                                                       as unique_unsubscribed,

             countStateIf(type = 'PROMO_CODE_RESERVED')                                                                                                                              as promo__codes_issued,
             countStateIf(type = 'PROMO_SUCCESS_PAGE_OPENED')                                                                                                                        as promo__success_page_open,
             countStateIf(type = 'PROMO_INVALID_PAGE_OPENED')                                                                                                                        as promo__invalid_page_open,

             countStateIf(type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 1))                                                                                      as promo__redeemed_1d,
             countStateIf(type = 'PROMO_REWARD_REJECTED' and date < addDays(scheduled_date, 1))                                                                                      as promo__rejected_1d,
             sumStateIf(if(promo_redeem_sales is null, toFloat32(0), assumeNotNull(promo_redeem_sales)),
                        type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 1))                                                                                        as promo__redemption_sales_1d,
             sumStateIf(if(promo_redeem_discount is null, toFloat32(0), assumeNotNull(promo_redeem_discount)), type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 1)) as promo__cost_1d,


             countStateIf(type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 7))                                                                                      as promo__redeemed_7d,
             countStateIf(type = 'PROMO_REWARD_REJECTED' and date < addDays(scheduled_date, 7))                                                                                      as promo__rejected_7d,
             sumStateIf(if(promo_redeem_sales is null, toFloat32(0), assumeNotNull(promo_redeem_sales)),
                        type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 7))                                                                                        as promo__redemption_sales_7d,
             sumStateIf(if(promo_redeem_discount is null, toFloat32(0), assumeNotNull(promo_redeem_discount)), type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 7)) as promo__cost_7d,


             countStateIf(type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 14))                                                                                     as promo__redeemed_14d,
             countStateIf(type = 'PROMO_REWARD_REJECTED' and date < addDays(scheduled_date, 14))                                                                                     as promo__rejected_14d,
             sumStateIf(if(promo_redeem_sales is null, toFloat32(0), assumeNotNull(promo_redeem_sales)),
                        type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 14))                                                                                       as promo__redemption_sales_14d,
             sumStateIf(if(promo_redeem_discount is null, toFloat32(0), assumeNotNull(promo_redeem_discount)),
                        type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 14))                                                                                       as promo__cost_14d,


             countStateIf(type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 21))                                                                                     as promo__redeemed_21d,
             countStateIf(type = 'PROMO_REWARD_REJECTED' and date < addDays(scheduled_date, 21))                                                                                     as promo__rejected_21d,
             sumStateIf(if(promo_redeem_sales is null, toFloat32(0), assumeNotNull(promo_redeem_sales)),
                        type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 21))                                                                                       as promo__redemption_sales_21d,
             sumStateIf(if(promo_redeem_discount is null, toFloat32(0), assumeNotNull(promo_redeem_discount)),
                        type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 21))                                                                                       as promo__cost_21d,


             countStateIf(type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 30))                                                                                     as promo__redeemed_30d,
             countStateIf(type = 'PROMO_REWARD_REJECTED' and date < addDays(scheduled_date, 30))                                                                                     as promo__rejected_30d,
             sumStateIf(if(promo_redeem_sales is null, toFloat32(0), assumeNotNull(promo_redeem_sales)),
                        type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 30))                                                                                       as promo__redemption_sales_30d,
             sumStateIf(if(promo_redeem_discount is null, toFloat32(0), assumeNotNull(promo_redeem_discount)),
                        type = 'PROMO_REWARD_REDEEMED' and date < addDays(scheduled_date, 30))                                                                                       as promo__cost_30d,


             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and date < addDays(scheduled_date, 1))                                               as txn__by_delivered_count_1d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and date < addDays(scheduled_date, 1))                                            as txn__by_delivered_sales_1d,
             countStateIf(type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and date < addDays(scheduled_date, 1))                                               as txn__by_open_count_1d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and date < addDays(scheduled_date, 1))                                                 as txn__by_open_sales_1d,
             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and date < addDays(scheduled_date, 1))                                                   as txn__by_click_count_1d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and date < addDays(scheduled_date, 1))                                                as txn__by_click_sales_1d,


             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and date < addDays(scheduled_date, 7))                                               as txn__by_delivered_count_7d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and date < addDays(scheduled_date, 7))                                            as txn__by_delivered_sales_7d,
             countStateIf(type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and date < addDays(scheduled_date, 7))                                               as txn__by_open_count_7d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and date < addDays(scheduled_date, 7))                                                 as txn__by_open_sales_7d,
             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and date < addDays(scheduled_date, 7))                                                   as txn__by_click_count_7d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and date < addDays(scheduled_date, 7))                                                as txn__by_click_sales_7d,


             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and date < addDays(scheduled_date, 14))                                              as txn__by_delivered_count_14d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and date < addDays(scheduled_date, 14))                                           as txn__by_delivered_sales_14d,
             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and date < addDays(scheduled_date, 14))                                                   as txn__by_open_count_14d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and date < addDays(scheduled_date, 14))                                                as txn__by_open_sales_14d,
             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and date < addDays(scheduled_date, 14))                                                  as txn__by_click_count_14d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and date < addDays(scheduled_date, 14))                                               as txn__by_click_sales_14d,


             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and date < addDays(scheduled_date, 21))                                              as txn__by_delivered_count_21d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and date < addDays(scheduled_date, 21))                                           as txn__by_delivered_sales_21d,
             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and date < addDays(scheduled_date, 21))                                                   as txn__by_open_count_21d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and date < addDays(scheduled_date, 21))                                                as txn__by_open_sales_21d,
             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and date < addDays(scheduled_date, 21))                                                  as txn__by_click_count_21d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and date < addDays(scheduled_date, 21))                                               as txn__by_click_sales_21d,


             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and date < addDays(scheduled_date, 30))                                              as txn__by_delivered_count_30d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_DELIVERED_EVENT' and date < addDays(scheduled_date, 30))                                           as txn__by_delivered_sales_30d,
             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and date < addDays(scheduled_date, 30))                                                   as txn__by_open_count_30d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_OPEN_EVENT' and date < addDays(scheduled_date, 30))                                                as txn__by_open_sales_30d,
             countStateIf(
                     type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and date < addDays(scheduled_date, 30))                                                  as txn__by_click_count_30d,
             sumStateIf(if(txn_sales is null, toFloat32(0), assumeNotNull(txn_sales)),
                        type = 'EMAIL_TRANSACTION' and txn_attribution_type = 'BY_CLICK_EVENT' and date < addDays(scheduled_date, 30))                                               as txn__by_click_sales_30d

      from email_stats.email_events_data

      where ((campaign_id is not null and audience_id is not null) or (legacy_campaign_id is not null and legacy_audience_group_id is not null))
        and is_test = 0
        and is_send_failed = 0

      group by date,
               type,
               brand_id,
               campaign_id,
               audience_id,
               legacy_campaign_id,
               legacy_audience_group_id,
               promotion_id,
               location_id
         );

