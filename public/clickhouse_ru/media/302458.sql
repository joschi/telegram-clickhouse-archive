WITH        
    '{
  "key": "OIP-2860",
  "changelog": {
    "histories": [
      {
        "author": {
          "displayName": "xxxx"
        },
        "created": "2022-11-27T16:13:42.971+0700",
        "items": [
          {
            "field": "Sprint",
            "fromString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41, OI Sprint FY22 42, OI Sprint FY22 43, OI Sprint FY22 44, OI Sprint FY22 45, OI Sprint FY22 46",
            "toString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41, OI Sprint FY22 42, OI Sprint FY22 43, OI Sprint FY22 44, OI Sprint FY22 45, OI Sprint FY22 46, OI Sprint FY22 47"
          }
        ]
      },
      {
        "author": {
          "displayName": "xxxx"
        },
        "created": "2022-11-21T13:18:03.032+0700",
        "items": [
          {
            "field": "Sprint",
            "fromString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41, OI Sprint FY22 42, OI Sprint FY22 43, OI Sprint FY22 44, OI Sprint FY22 45",
            "toString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41, OI Sprint FY22 42, OI Sprint FY22 43, OI Sprint FY22 44, OI Sprint FY22 45, OI Sprint FY22 46"
          }
        ]
      },
      {
        "author": {
          "displayName": "xxxx"
        },
        "created": "2022-11-12T12:58:26.903+0700",
        "items": [
          {
            "field": "Sprint",
            "fromString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41, OI Sprint FY22 42, OI Sprint FY22 43, OI Sprint FY22 44",
            "toString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41, OI Sprint FY22 42, OI Sprint FY22 43, OI Sprint FY22 44, OI Sprint FY22 45"
          }
        ]
      },
      {
        "author": {
          "displayName": "xxxx"
        },
        "created": "2022-11-05T13:28:20.133+0700",
        "items": [
          {
            "field": "Sprint",
            "fromString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41, OI Sprint FY22 42, OI Sprint FY22 43",
            "toString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41, OI Sprint FY22 42, OI Sprint FY22 43, OI Sprint FY22 44"
          }
        ]
      },
      {
        "author": {
          "displayName": "xxxx"
        },
        "created": "2022-10-29T15:24:26.902+0700",
        "items": [
          {
            "field": "Sprint",
            "fromString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41, OI Sprint FY22 42",
            "toString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41, OI Sprint FY22 42, OI Sprint FY22 43"
          }
        ]
      },
      {
        "author": {
          "displayName": "xxxx"
        },
        "created": "2022-10-21T22:24:43.493+0700",
        "items": [
          {
            "field": "Sprint",
            "fromString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41",
            "toString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41, OI Sprint FY22 42"
          }
        ]
      },
      {
        "author": {
          "displayName": "xxxx"
        },
        "created": "2022-10-13T22:42:11.115+0700",
        "items": [
          {
            "field": "Sprint",
            "fromString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40",
            "toString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40, OI Sprint FY22 41"
          }
        ]
      },
      {
        "author": {
          "displayName": "yyyyy"
        },
        "created": "2022-10-07T12:21:25.522+0700",
        "items": [
          {
            "field": "Sprint",
            "fromString": "OI Sprint FY22 38, OI Sprint FY22 39",
            "toString": "OI Sprint FY22 38, OI Sprint FY22 39, OI Sprint FY22 40"
          }
        ]
      },
      {
        "author": {
          "displayName": "xxxx"
        },
        "created": "2022-09-30T01:24:06.151+0700",
        "items": [
          {
            "field": "Sprint",
            "fromString": "OI Sprint FY22 38",
            "toString": "OI Sprint FY22 38, OI Sprint FY22 39"
          }
        ]
      },
      {
        "author": {
          "displayName": "zzz"
        },
        "created": "2022-09-23T13:17:01.618+0700",
        "items": [
          {
            "field": "Sprint",
            "fromString": "",
            "toString": "OI Sprint FY22 38"
          }
        ]
      }
    ]
  }
}'	as _airbyte_data,
    			JSONExtract(_airbyte_data, 'changelog', 'histories', 
                    'Array(
                        Tuple(
                            author Tuple(displayName String),
                            created String, 
                            items Array(
                                Tuple(
                                    field String,
                                    fromString String,
                                    toString String ))))')                                                              as changelog,
                arraySort( x -> x.1,
                    arrayMap( x ->
                        (
                            parseDateTimeBestEffort(x.1),
                            splitByString(', ', x.3)[-1],
                            splitByString(', ', x.4)[-1]
                        ),
                        arrayFilter(z -> z.2 <> '',
                                arrayMap(x ->
                                    (   x.2,
                                        untuple(arrayFilter( y -> y.1 = 'Sprint', x.3)[1])
                                    ), 
                                    changelog)
                        )
                    )
                )                                                                                                           as bag_field
    SELECT      
--		toTypeName(bag_field) as `type`,
        bag_field as rrr,
--		bag_field as bbb,
        1