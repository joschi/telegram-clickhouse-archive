WITH
			67783						AS aid,
			6 								AS tax_rate,
			[137363668, 139055142, 144927974, 152939400, 144927915, 139059431, 144927741, 144927826, 152939398, 139088086, 137363591, 152939397, 144611348, 140313557, 155952458]	AS nmids,
			toDate('2023-05-08')					AS since,
			toDate('2023-05-11')					AS until,
			toDate('2023-05-04')				AS speedSince,
			toDate('2023-05-10')				AS speedUntil,
			speedUntil - speedSince + 1			AS periodLength
		SELECT
			allNms.nmId AS nmid,

			-- SPEED
			speedSince														AS spd_period_since,
			speedUntil														AS spd_period_until,
			periodLength													AS spd_period_source_length,
			speed_metrics.ordersCount										AS spd_orders_count,
			absentDaysCount													AS spd_absent_days_count,
			(periodLength - absentDaysCount)								AS spd_present_days_count,
			(spd_period_source_length - absentDaysCount)					AS spd_period_length,
			round(spd_orders_count / spd_present_days_count, 4)				AS spd_order_speed_count,
			round(speed_metrics.ordersAmount / spd_present_days_count, 4)	AS spd_orders_speed_amount,
			round(spd_orders_speed_amount * absentDaysCount, 4)				AS spd_lost_profit,

			-- REPORT
			orders_count,
			orders_amount,
			sales_count,
			sales_amount,

			-- From stocks_v1
			stocksV1.inWayToClient		AS stocks_to_clients,
			stocksV1.daysOnSite			AS days_on_site,
			stocksV1.inSaleSince		AS in_sale_since,
			stocksV1.supplierArticle	AS supplier_article,

			-- Actual prices
			prices.price					AS price,
			prices.resultPrice				AS result_price,
			prices.discountBasicPercent		AS discount_basic_percent,
			prices.discountBasicAmount		AS discount_basic_amount,
			prices.discountPromoPercent		AS discount_promo_percent,
			prices.discountPromoAmount		AS discount_promo_amount,
			prices.sppPercent				AS spp_percent,
			prices.sppAmount				AS spp_amount,
			prices.resultPriceWithSpp		AS result_price_with_spp,

			-- Actual stocks
			stocks.fboQuantity							AS stocks_quantity_fbo,
			stocks.fboAmount							AS stocks_amount_fbo,
			stocks.fbsQuantity							AS stocks_quantity_fbs,
			stocks.fbsAmount							AS stocks_amount_fbs,
			(stocks.fboQuantity + stocks.fbsQuantity)	AS stocks_quantity,

			floor(stocks_quantity_fbo / spd_order_speed_count)	AS enough_for_days,
			floor(stocks_quantity_fbs / spd_order_speed_count)	AS enough_for_days_fbs,

			-- Profit
			custom.primeCost 					AS primeCost,
			sales_count * primeCost				AS primeCostAmount,
      		
      		if(primeCostAmount != 0, tProfit.salesProfit - (canceled_count  * 33), NULL) 			AS profit,
       		if(primeCostAmount != 0 AND isNotNull(profit), profit / primeCostAmount * 100, NULL) 								AS roi,
       		if(sales_amount != 0 AND isNotNull(profit), profit / sales_amount * 100, NULL) 	AS profitability,

			1 as one
		FROM (SELECT arrayJoin(nmids) as nmId) as allNms
		LEFT JOIN (SELECT nmId, count() as orders_count, countIf(isCancel = 1) as canceled_count, sum(price) as orders_amount FROM (
            SELECT nmId,odid,anyLast(price) AS price,anyLast(isCancel) AS isCancel
            FROM (SELECT nmId, odid, price, isCancel FROM wb_normalized.orders_v1_index WHERE accountId = aid AND date BETWEEN since AND until AND nmId IN nmids ORDER BY version ASC)
            GROUP BY nmId, odid
        ) GROUP BY nmId) AS ordersMetrics ON ordersMetrics.nmId = allNms.nmId
		LEFT JOIN (SELECT nmId, sum(profit) as salesProfit, countIf(type='s') as sales_count, sumIf(priceWithDisc, type='s') as sales_amount, countIf(type='r') as returns_count FROM (
			SELECT 
			    sales.nmId as nmId,
			    sales.date as date,
			    sales.optionId as optionId,
			    
			    type,
			    finishedPrice,
				priceWithDisc,
			    round(((finishedPrice - forPay) / finishedPrice * 100), 2) AS commission, 
			    custom.primeCost AS primeCost, 
			    round((finishedPrice / 100 * 6), 2) AS taxAmount,
			    volume,
			    if(warehousesTariffs.logisticsToBase = 0, warehousesTariffsToday.logisticsToBase, warehousesTariffs.logisticsToBase) AS logisticsToBase,
				if(warehousesTariffs.logisticsToAdditional = 0, warehousesTariffsToday.logisticsToAdditional, warehousesTariffs.logisticsToAdditional) AS logisticsToAdditional,
				if(warehousesTariffs.logisticsFrom = 0, warehousesTariffsToday.logisticsFrom, warehousesTariffs.logisticsFrom) AS logisticsFrom,
				round(if(volume <= 5, logisticsToBase, logisticsToBase + ((volume - 5) * logisticsToAdditional)), 1) AS logisticCostByVolume,
				if(isOversize = 1 AND logisticCostByVolume < 1000, 1000, logisticCostByVolume) AS logisticCost,
				if(forPay != 0 AND primeCost != 0, if(type IN ('s'), round(forPay - logisticCost - primeCost - taxAmount, 2), -33), 0) AS profit
			FROM (
            SELECT nmId,type,odid,max(optionId) AS optionId,anyLast(warehouseId) AS warehouseId,anyLast(forPay) AS forPay,anyLast(finishedPrice) AS finishedPrice,anyLast(date) AS date,anyLast(priceWithDisc) AS priceWithDisc
            FROM (SELECT nmId, type, odid, optionId, if(warehouseId = 1699, 130744, warehouseId) as warehouseId, forPay, finishedPrice, date, priceWithDisc FROM wb_normalized.sales_v1_by_aid WHERE accountId = 67783 AND date between since and until AND nmId in nmids AND type in ('s', 'r') ORDER BY version ASC)
            GROUP BY nmId, type, odid
        ) as sales
		
		LEFT JOIN (
			SELECT
				nmId,
				anyLast(primeCost) 				AS primeCost,
				anyLast(deficitDivisor) 		AS deficitDivisor,
				anyLast(deficitDivisorType) 	AS deficitDivisorType
			FROM (
				SELECT nmId, primeCost, deficitDivisor, deficitDivisorType
				FROM wb_completed.products_custom_values
				WHERE nmId IN nmids
				ORDER BY version ASC
			) GROUP BY nmId
		) AS custom ON custom.nmId = sales.nmId
		 LEFT JOIN (
		-- Объём товара (productVolumeTemplate)
			SELECT anyLast(oversized) AS isOversize,nmId,anyLast(round(volume, 2)) as volume
        	FROM (
        		SELECT *
        		FROM wb_completed.product_option_volume 
        		WHERE nmId IN nmids AND date between dateSub(day, 3, today()) and today()
        		ORDER BY date, version ASC)
    		GROUP BY nmId
		-- END Объём товара (productVolumeTemplate)
		) AS productVolume ON sales.nmId = productVolume.nmId  LEFT JOIN ( SELECT logisticsToBase, logisticsToAdditional, logisticsFrom, date, warehouseId FROM (
				SELECT
        			date,
        			if(warehouseId = 1699, 130744, warehouseId) as warehouseId,
        			anyLast(logisticsToBase) as logisticsToBase,
        			anyLast(logisticsToAdditional) as logisticsToAdditional,
        			anyLast(logisticsFrom) as logisticsFrom,
        			anyLast(storageBase) as storageBase,
        			anyLast(storageAdditional) as storageAdditional,
        			anyLast(acceptanceBase) as acceptanceBase,
        			anyLast(acceptanceAdditional) as acceptanceAdditional
        		FROM (
        			SELECT *
        			FROM wb_completed.warehouses_tariffs
        			WHERE date BETWEEN since AND until
        			ORDER BY date ASC, warehouseId ASC, version ASC
        		)
        		GROUP BY date, warehouseId
        		ORDER BY date
        	)  ) AS warehousesTariffs ON sales.date = warehousesTariffs.date AND sales.warehouseId = warehousesTariffs.warehouseId  LEFT JOIN ( SELECT logisticsToBase, logisticsToAdditional, logisticsFrom, date, warehouseId FROM (
				SELECT
        			date,
        			if(warehouseId = 1699, 130744, warehouseId) as warehouseId,
        			anyLast(logisticsToBase) as logisticsToBase,
        			anyLast(logisticsToAdditional) as logisticsToAdditional,
        			anyLast(logisticsFrom) as logisticsFrom,
        			anyLast(storageBase) as storageBase,
        			anyLast(storageAdditional) as storageAdditional,
        			anyLast(acceptanceBase) as acceptanceBase,
        			anyLast(acceptanceAdditional) as acceptanceAdditional
        		FROM (
        			SELECT *
        			FROM wb_completed.warehouses_tariffs
        			WHERE date = today()
        			ORDER BY date ASC, warehouseId ASC, version ASC
        		)
        		GROUP BY date, warehouseId
        		ORDER BY date
        	)  ) AS warehousesTariffsToday ON sales.warehouseId = warehousesTariffsToday.warehouseId ) GROUP BY nmId) AS tProfit ON tProfit.nmId = allNms.nmId
		LEFT JOIN (SELECT nmId, absentDaysCount, ordersCount, ordersAmount FROM (
			SELECT
				nmId,
				
				sum(absentDay)	as absentDaysCount,
				sum(quantity)	as ordersCount,
				sum(amount)     as ordersAmount
			FROM (
				SELECT 
					allNms.nmId as nmId,
					toDate(allDates.date) as date,
					quantity,
					amount,
					
					if(outOfStocks = 1 AND quantity = 0, 1, 0) as absentDay
				FROM (SELECT arrayJoin(nmids) as nmId) AS allNms,
					 (SELECT toDate('2023-05-04') + INTERVAL number DAY as date
			          FROM numbers(toUInt64(dateDiff('day', toDate('2023-05-04'), toDate('2023-05-10')) + 1))
			         ) AS allDates
				-- сводка по наличию
				LEFT JOIN (
			        SELECT nmId, date, anyLast(outOfStocks) AS outOfStocks
			        FROM (SELECT nmId, date, outOfStocks
			              FROM wb_completed.product_summary_out_of_stocks_by_nmid
                          WHERE date between '2023-05-04' and '2023-05-10' AND nmId IN nmids
                          ORDER BY version ASC
                    )
                    GROUP BY date, nmId
                ) as stocks ON stocks.nmId = allNms.nmId AND stocks.date = toDate(allDates.date)
                    
                -- сводка по заказам
                LEFT JOIN (
                	SELECT nmId, count() as quantity, sum(price) as amount, date
                  	FROM (
            SELECT nmId,odid,anyLast(date) AS date,anyLast(price) AS price
            FROM (SELECT nmId, odid, date, price FROM wb_normalized.orders_v1_index WHERE accountId = 67783 AND date between '2023-05-04' and '2023-05-10' AND nmId in nmids AND isCancel = 0 ORDER BY version ASC)
            GROUP BY nmId, odid
        )
                    GROUP BY nmId, date
                ) AS orders ON orders.nmId = allNms.nmId AND orders.date = toDate(allDates.date)
                
                ORDER BY allNms.nmId, date
            ) GROUP BY nmId
        )) AS speed_metrics ON speed_metrics.nmId = allNms.nmId
		
		LEFT JOIN (
			SELECT
				nmId,max(sppPercent) AS sppPercent,max(sppAmount) AS sppAmount,max(resultPriceWithSpp) AS resultPriceWithSpp,max(price)					AS price,max(resultPrice)			AS resultPrice,max(discountBasicPercent)	AS discountBasicPercent,max(discountBasicAmount)	AS discountBasicAmount,max(discountPromoPercent)	AS discountPromoPercent,max(discountPromoAmount)	AS discountPromoAmount
			FROM (
				-- Этот селект нужен для пропуска несхлопнутых записей
				SELECT
					nmId, optionId,
					anyLast(price) AS price,
					anyLast(resultPrice) AS resultPrice,
					anyLast(discountBasicPercent) AS discountBasicPercent,
					anyLast(discountBasicAmount) AS discountBasicAmount,
					anyLast(discountPromoPercent) AS discountPromoPercent,
					anyLast(discountPromoAmount) AS discountPromoAmount,
					anyLast(sppPercent) AS sppPercent,
					anyLast(sppAmount) AS sppAmount,
					anyLast(resultPriceWithSpp) AS resultPriceWithSpp
				FROM (
					SELECT *, 
					spp AS sppPercent,
					if(spp > 0, resultPrice / 100 * sppPercent, 0) AS sppAmount,
					resultPrice - sppAmount AS resultPriceWithSpp  
					FROM wb_completed.products_prices
					WHERE nmId IN nmids
					ORDER BY version ASC
				) GROUP BY nmId, optionId
				-- Этот селект нужен для пропуска несхлопнутых записей
			)
			GROUP BY nmId
		) AS prices ON prices.nmId = allNms.nmId
		
		LEFT JOIN (
			SELECT
				nmId,any(resultPrice)															AS resultPrice,sumIf(quantity, warehouseId IN (507, 686, 1193, 1699, 1733, 2737, 115577, 116433, 117501, 117544, 117986, 120762, 121709, 124731, 130744, 159402, 161812, 204939, 205228, 206348, 206968, 207743, 208277, 208941, 210001, 210515, 211622))		AS fboQuantity,sumIf(quantity, warehouseId NOT IN (507, 686, 1193, 1699, 1733, 2737, 115577, 116433, 117501, 117544, 117986, 120762, 121709, 124731, 130744, 159402, 161812, 204939, 205228, 206348, 206968, 207743, 208277, 208941, 210001, 210515, 211622))	AS fbsQuantity,(fboQuantity * resultPrice)												AS fboAmount,(fbsQuantity * resultPrice)												AS fbsAmount
			FROM (
				-- Этот селект нужен для пропуска несхлопнутых записей
				SELECT
					nmId, optionId, warehouseId,
					anyLast(quantity) AS quantity,
					anyLast(resultPrice) AS resultPrice
				FROM (
					SELECT nmId, optionId, warehouseId, quantity, resultPrice
					FROM wb_completed.products_quantity_price
					WHERE nmId IN nmids
					ORDER BY version ASC
				) GROUP BY nmId, optionId, warehouseId
				-- Этот селект нужен для пропуска несхлопнутых записей
			) GROUP BY nmId
		) AS stocks ON stocks.nmId = allNms.nmId
		
		LEFT JOIN (
			SELECT
				nmId,sum(quantity)													AS quantity_v1,anyLast(price)													AS price_v1,anyLast(discount)												AS discount_v1,floor(price_v1 - price_v1 * discount_v1 / 100)					AS resultPrice_v1,sum(inWayToClient)												AS inWayToClient,anyLast(daysOnSite)											AS daysOnSite,if(daysOnSite > 0, toDate(now() - daysOnSite * 86400), null)	AS inSaleSince,anyLast(barcode)												AS barcode,(quantity_v1 * resultPrice_v1)									AS stockAmount_v1,anyLast(supplierArticle)										AS supplierArticle
			FROM (
				-- Этот селект нужен для пропуска несхлопнутых записей
				SELECT
					nmId, optionId, warehouseId,
					anyLast(quantity)			AS quantity,
					anyLast(price)				AS price,
					anyLast(discount)			AS discount,
					anyLast(inWayToClient)		AS inWayToClient,
					anyLast(daysOnSite)			AS daysOnSite,
					anyLast(barcode)			AS barcode,
					anyLast(supplierArticle)	AS supplierArticle
				FROM (
					SELECT nmId, optionId, warehouseId, quantity, price, discount,
					quantityFull - quantity as inWayToClient, daysOnSite, barcode, supplierArticle
					FROM wb_normalized.stocks_v1
					WHERE accountId IN aid AND nmId IN nmids
					ORDER BY version ASC
				)
				GROUP BY nmId, optionId, warehouseId
				-- Этот селект нужен для пропуска несхлопнутых записей
			)
			GROUP BY nmId
		) AS stocksV1 ON stocksV1.nmId = allNms.nmId
		
		LEFT JOIN (
			SELECT
				nmId,
				anyLast(primeCost) 				AS primeCost,
				anyLast(deficitDivisor) 		AS deficitDivisor,
				anyLast(deficitDivisorType) 	AS deficitDivisorType
			FROM (
				SELECT nmId, primeCost, deficitDivisor, deficitDivisorType
				FROM wb_completed.products_custom_values
				WHERE nmId IN nmids
				ORDER BY version ASC
			) GROUP BY nmId
		) AS custom ON custom.nmId = allNms.nmId
		ORDER BY orders_count, nmid;
