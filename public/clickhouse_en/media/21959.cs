﻿using System;
using System.Data;
using ClickHouse.Ado;

namespace TestCKQuery
{

    class Program
    {
        private static ClickHouseConnection GetConnection(
        string cstr = "Host=xxx;Port=9000;User=default;Password=xxx;Database=default;SocketTimeout=30000;")
        {
            var settings = new ClickHouseConnectionSettings(cstr);
            var cnn = new ClickHouseConnection(settings);
            cnn.Open();
            return cnn;
        }

        static void Main(string[] args)
        {
            try
            {
                QueryCK();
                Console.ReadKey();
            }
            catch
            {
                throw;
            }
        }

        private static void PrintData(IDataReader reader)
        {
            do
            {
                while (reader.Read())
                {
                    for (var i = 0; i < reader.FieldCount; i++)
                    {
                        var val = reader.GetValue(i);
                        Console.Write(val);
                        Console.Write(", ");
                    }
                    Console.WriteLine();
                }
            } while (reader.NextResult());
        }

        private static void QueryCK()
        {

            using (var cnn = GetConnection())
            {

                var ddsql = "set insert_deduplicate=0";
                cnn.CreateCommand(ddsql).ExecuteNonQuery();

                //var sql = "alter table default.table_name3 delete where 1=1";
                //cnn.CreateCommand(sql).ExecuteNonQuery();
                var sql = "insert into default.table_name3 values('2020-04-04 10:10:11',1,22222)";

                cnn.CreateCommand(sql).ExecuteNonQuery();
                using (var reader = cnn.CreateCommand("select * from default.table_name3 srav ").ExecuteReader())
                {
                    PrintData(reader);
                    Console.WriteLine("success");
                }
            }

        }
    }
}
