apiVersion: "clickhouse.altinity.com/v1"
kind: "ClickHouseInstallation"
metadata:
  name: "clickhouse-prime"
  labels:
    label1: label1_value
  annotations:
    annotation1: annotation1_value

spec:
  defaults:
    replicasUseFQDN: "no"
    distributedDDL:
      profile: default
    templates:
      podTemplate: clickhouse-v20.3
      dataVolumeClaimTemplate: volume-claim-retain-pvc
      logVolumeClaimTemplate: default-volume-claim
      serviceTemplate: chi-service-template

  configuration:
    zookeeper:
      nodes:
        - host: <some public ip>
          port: 2181
      session_timeout_ms: 30000
      operation_timeout_ms: 10000
    users:
      default/profile: default
      readonly/profile: readonly
      default/networks/ip:
        - "127.0.0.1"
        - "192.168.0.0/24"
        - "::1"
        - "::/0"
      prime/networks/ip:
        - "127.0.0.1"
        - "::/0"
      prime/profile: default
      prime/quotas: default
    profiles:
      readonly/readonly: "1"
      default/max_memory_usage: "1000000000"
    quotas:
      default/interval/duration: "3600"
    settings:
      compression/case/method: zstd
      disable_internal_dns_cache: 1

    clusters:
      - name: production
        templates:
          podTemplate: clickhouse-v20.3
          dataVolumeClaimTemplate: volume-claim-retain-pvc
          logVolumeClaimTemplate: default-volume-claim
        layout:
          shards:
            - name: shard0
              internalReplication: Enabled
              replicas:
                - name: shard0-replica0
                  tcpPort: 9000
                  httpPort: 8123
                  interserverHTTPPort: 9009
                  templates:
                    podTemplate: clickhouse-v20.3
                    dataVolumeClaimTemplate: volume-claim-retain-pvc
                    logVolumeClaimTemplate: default-volume-claim
                    replicaServiceTemplate: replica-service-template
                - name: shard0-replica1
                  tcpPort: 9000
                  httpPort: 8123
                  interserverHTTPPort: 9009
                  templates:
                    podTemplate: clickhouse-replica-v20.3
                    dataVolumeClaimTemplate: volume-claim-delete-pvc
                    logVolumeClaimTemplate: default-volume-claim
                    replicaServiceTemplate: replica-service-template

            - name: shard1
              internalReplication: Enabled
              replicas:
                - name: shard1-replica0
                  tcpPort: 9000
                  httpPort: 8123
                  interserverHTTPPort: 9009
                  templates:
                    podTemplate: clickhouse-v20.3
                    dataVolumeClaimTemplate: volume-claim-retain-pvc
                    logVolumeClaimTemplate: default-volume-claim
                    replicaServiceTemplate: replica-service-template
                - name: shard1-replica1
                  tcpPort: 9000
                  httpPort: 8123
                  interserverHTTPPort: 9009
                  templates:
                    podTemplate: clickhouse-replica-v20.3
                    dataVolumeClaimTemplate: volume-claim-delete-pvc
                    logVolumeClaimTemplate: default-volume-claim
                    replicaServiceTemplate: replica-service-template

  templates:
    serviceTemplates:
      - name: chi-service-template
        generateName: "service-{chi}"
        metadata:
          labels:
            custom.label: "custom.value"
          annotations:
            # For more details on Internal Load Balancer check
            # https://kubernetes.io/docs/concepts/services-networking/service/#internal-load-balancer
            cloud.google.com/load-balancer-type: "Internal"
            service.beta.kubernetes.io/aws-load-balancer-internal: "true"
            service.beta.kubernetes.io/azure-load-balancer-internal: "true"
            service.beta.kubernetes.io/openstack-internal-load-balancer: "true"
            service.beta.kubernetes.io/cce-load-balancer-internal-vpc: "true"

            # NLB Load Balancer
            service.beta.kubernetes.io/aws-load-balancer-type: "nlb"
        spec:
          ports:
            - name: http
              port: 8123
            - name: tcp
              port: 9000
          type: LoadBalancer

      - name: replica-service-template
        # type ServiceSpec struct from k8s.io/core/v1
        spec:
          ports:
            - name: http
              port: 8123
            - name: tcp
              port: 9000
            - name: interserver
              port: 9009
          type: ClusterIP
          clusterIP: None


    volumeClaimTemplates:
      - name: default-volume-claim
        spec:
          storageClassName: gp2-delete
          accessModes:
            - ReadWriteOnce
          resources:
            requests:
              storage: 1Gi
      - name: volume-claim-delete-pvc
        reclaimPolicy: Delete
        spec:
          storageClassName: gp2-delete
          accessModes:
            - ReadWriteOnce
          resources:
            requests:
              storage: 1Gi
      - name: volume-claim-retain-pvc
        reclaimPolicy: Retain
        spec:
          storageClassName: gp2-persistent
          accessModes:
            - ReadWriteOnce
          resources:
            requests:
              storage: 1Gi

    podTemplates:
      - name: clickhouse-v20.3
        zone:
          key: "node-lifecycle"
          values:
            - "on-demand"
        podDistribution:
          - type: ClickHouseAntiAffinity
          - type: ShardAntiAffinity
          - type: MaxNumberPerNode
            number: 2

        spec:
          containers:
            - name: clickhouse
              image: yandex/clickhouse-server:20.3
              volumeMounts:
                - name: volume-claim-retain-pvc
                  mountPath: /var/lib/clickhouse
              resources:
                requests:
                  memory: "250Mi"
                  cpu: "250m"
                limits:
                  memory: "500Mi"
                  cpu: "500m"
      - name: clickhouse-replica-v20.3
        zone:
          key: "node-lifecycle"
          values:
            - "spot"
        podDistribution:
          - type: ClickHouseAntiAffinity
          - type: ShardAntiAffinity
          - type: MaxNumberPerNode
            number: 2
        spec:
          containers:
            - name: clickhouse
              image: yandex/clickhouse-server:20.3
              volumeMounts:
                - name: volume-claim-delete-pvc
                  mountPath: /var/lib/clickhouse
              resources:
                requests:
                  memory: "250Mi"
                  cpu: "250m"
                limits:
                  memory: "500Mi"
                  cpu: "500m"

