ATTACH TABLE searchlog
(
    `AnonymousId` Nullable(String),
    `ApplicationID` UInt8,
    `Application` LowCardinality(String) MATERIALIZED dictGet('datascience.Enums', 'Name', ('ApplicationType', toInt32(ApplicationID))),
    `DeviceID` LowCardinality(String),
    `EnginesFound` Array(UInt16),
    `MatchingItineraryCount` UInt16,
    `AirlinesFound` Array(LowCardinality(String)),
    `EnginesFoundNames` Array(String) MATERIALIZED arrayMap(x -> dictGet('datascience.Enums', 'Name', ('EngineType', toInt32(x))), EnginesFound),
    `FpAffiliate` LowCardinality(String),
    `AffiliateGroup` LowCardinality(String) MATERIALIZED dictGet('references.AffiliateGroup', 'AffiliateGroup', tuple(lower(FpAffiliate))),
    `FpSubAffiliate` LowCardinality(String),
    `HasSameDateSameOndResults` UInt8,
    `IsBooked` UInt8,
    `MachineName` LowCardinality(String),
    `Pax_Adults` UInt8,
    `Pax_Children` UInt8,
    `PersonGuid` Nullable(UUID),
    `Portal` UInt16,
    `PortalName` LowCardinality(String) MATERIALIZED dictGet('datascience.Enums', 'Name', ('ApplicaitonPortals', toInt32(Portal))),
    `PortalCurrency` LowCardinality(String),
    `SearchClassOfService` LowCardinality(String),
    `SearchDepartureDate` Date,
    `SearchDepartureFrom` LowCardinality(String),
    `SearchDepartureFromCity` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'm_city', tuple(SearchDepartureFrom)),
    `SearchDepartureFromState` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'state_code', tuple(SearchDepartureFrom)),
    `SearchDepartureFromCountry` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'country_code', tuple(SearchDepartureFrom)),
    `SearchDepartureTo` LowCardinality(String),
    `SearchDepartureToCity` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'm_city', tuple(SearchDepartureTo)),
    `SearchDepartureToState` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'state_code', tuple(SearchDepartureTo)),
    `SearchDepartureToCountry` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'country_code', tuple(SearchDepartureTo)),
    `SearchGuid` UUID,
    `SearchReturnDate` Nullable(Date),
    `SearchReturnFrom` LowCardinality(String),
    `SearchReturnFromCity` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'm_city', tuple(SearchReturnFrom)),
    `SearchReturnFromState` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'state_code', tuple(SearchReturnFrom)),
    `SearchReturnFromCountry` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'country_code', tuple(SearchReturnFrom)),
    `SearchReturnTo` LowCardinality(String),
    `SearchReturnToCity` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'm_city', tuple(SearchReturnTo)),
    `SearchReturnToState` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'state_code', tuple(SearchReturnTo)),
    `SearchReturnToCountry` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'country_code', tuple(SearchReturnTo)),
    `SearchSource` LowCardinality(String),
    `SearchDate` Date,
    `SearchTime` DateTime('America/New_York'),
    `SearchTypeOfTrip` LowCardinality(String),
    `SessionId` Nullable(String),
    `TransactionID` Int64,
    `TotalContractsSearch` UInt16,
    `Airlines` Array(String),
    `BestFor` Array(LowCardinality(String)),
    `BookingEngine` Int16,
    `BookingEngineName` LowCardinality(String) MATERIALIZED dictGet('datascience.Enums', 'Name', ('EngineType', toInt32(BookingEngine))),
    `ContractBookedTIDs` Array(Int64),
    `ContractBookedIDs` Array(Int16),
    `ContractIDs` Array(Int16),
    `NumberOfContractsRow` UInt16 MATERIALIZED length(ContractIDs),
    `CostPerPax` Nullable(Float32),
    `CreditCardFeesPerPax` Nullable(Float32),
    `Discount` Nullable(Float32),
    `DisplayPrice` Float32,
    `FareType` Int8,
    `FlightClass` UInt8,
    `FlightDepartureAirlines` Array(LowCardinality(String)),
    `FlightDepartureArrivalTime` Array(DateTime('America/New_York')),
    `FlightDepartureDepTime` Array(DateTime('America/New_York')),
    `FlightDepartureDepDate` Array(Date) MATERIALIZED arrayMap(x -> toDate(x, 'America/New_York'), FlightDepartureDepTime),
    `FlightDepartureDepTimeString` Array(String) MATERIALIZED arrayMap(x -> formatDateTime(x, '%R', 'America/New_York'), FlightDepartureDepTime),
    `FlightDepartureFlightClass` Array(LowCardinality(String)),
    `FlightDepartureFlightDuration` Int16,
    `FlightDepartureFrom` LowCardinality(String),
    `FlightDepartureFromCity` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'm_city', tuple(FlightDepartureFrom)),
    `FlightDepartureFromState` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'state_code', tuple(FlightDepartureFrom)),
    `FlightDepartureFromCountry` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'country_code', tuple(FlightDepartureFrom)),
    `FlightDepartureLayoverDuration` Array(Int16),
    `FlightDepartureStops` UInt8,
    `FlightDepartureTo` LowCardinality(String),
    `FlightDepartureToCity` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'm_city', tuple(FlightDepartureTo)),
    `FlightDepartureToState` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'state_code', tuple(FlightDepartureTo)),
    `FlightDepartureToCountry` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'country_code', tuple(FlightDepartureTo)),
    `FlightReturnAirlines` Array(LowCardinality(String)),
    `FlightReturnArrivalTime` Array(DateTime('America/New_York')),
    `FlightReturnDepTime` Array(DateTime('America/New_York')),
    `FlightReturnDepDate` Array(Date) MATERIALIZED arrayMap(x -> toDate(x, 'America/New_York'), FlightReturnDepTime),
    `FlightReturnDepTimeString` Array(String) MATERIALIZED arrayMap(x -> formatDateTime(x, '%R', 'America/New_York'), FlightReturnDepTime),
    `FlightReturnFlightClass` Array(LowCardinality(String)),
    `FlightReturnFlightDuration` Nullable(Int16),
    `FlightReturnFrom` LowCardinality(String),
    `FlightReturnFromCity` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'm_city', tuple(FlightReturnFrom)),
    `FlightReturnFromState` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'state_code', tuple(FlightReturnFrom)),
    `FlightReturnFromCountry` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'country_code', tuple(FlightReturnFrom)),
    `FlightReturnLayoverDuration` Array(Int16),
    `FlightReturnStops` Nullable(UInt8),
    `FlightReturnTo` LowCardinality(String),
    `FlightReturnToCity` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'm_city', tuple(FlightReturnTo)),
    `FlightReturnToState` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'state_code', tuple(FlightReturnTo)),
    `FlightReturnToCountry` LowCardinality(String) MATERIALIZED dictGet('datascience.ReferenceAirports', 'country_code', tuple(FlightReturnTo)),
    `IsAlternateDate` UInt8,
    `IsCheapestAlt` UInt8,
    `IsCheapestAlternateDate` UInt8,
    `IsCheapestDirect` UInt8,
    `IsCheapestGlobal` UInt8,
    `IsCheapestNearby` UInt8,
    `IsCheapestSameDateSameOnD` UInt8,
    `IsFastestAlt` UInt8,
    `IsFastestGlobal` UInt8,
    `IsFastestSameDateSameOnD` UInt8,
    `IsNearbyAirport` UInt8,
    `IsOpaque` UInt8,
    `IsSRI` UInt8,
    `MatchingItineraryInfo` LowCardinality(String),
    `Markup` Float32,
    `PriceTotal` Float32,
    `ProfitPerPax` Float32,
    `ServiceFee` Float32,
    `SupplierCode` LowCardinality(String),
    `TotalFlightDuration` Int16,
    `TotalLayoverDuration` Array(Int16),
    `TotalPax` UInt8,
    `TotalStops` UInt8,
    `TotalTravelDuration` Array(Int16),
    `ValidatingCarrier` LowCardinality(String),
    `YMSString` Array(LowCardinality(String))
)
ENGINE = ReplicatedMergeTree('/clickhouse/tables/replicated/searchlog', 'serverhostname')
PARTITION BY SearchDate
ORDER BY (SearchDepartureFrom, SearchDepartureTo, SearchGuid, DisplayPrice)
SETTINGS index_granularity = 8192
