create table table_call_lte_tsel_20200306 (
        CALL_ID Int64,
        START_TIME Nullable(DateTime),
        START_TIME_MS Nullable(Int32),
        END_TIME Nullable(DateTime),
        END_TIME_MS Nullable(Int32),
        DURATION Nullable(Int32),
        IMSI Nullable(String),
        IMEI Nullable(String),
        RRC_REQUEST_TYPE Nullable(Int32),
        CALL_TYPE Nullable(UInt8),
        CALL_STATUS Nullable(UInt8),
        RRC_CONNECTION_REJECT Nullable(Int32),
        ERAB_SETUP_FAILURE Nullable(Int32),
        RRC_FAILURE_LAST Nullable(Int32),
        RRC_FAILURE_CAUSE_LAST Nullable(Int32),
        S1AP_FAILURE_LAST Nullable(Int32),
        S1AP_FAILURE_CAUSE_LAST Nullable(Int32),
        X2AP_FAILURE_LAST Nullable(Int32),
        X2AP_FAILURE_CAUSE_LAST Nullable(Int32),
        NAS_EMM_FAILURE_LAST Nullable(Int32),
        NAS_ESM_FAILURE_LAST Nullable(Int32),
        RRC_RELEASE_CAUSE Nullable(Int32),
        UL_VOLUME Nullable(Float64),
        DL_VOLUME Nullable(Float64),
        UL_THROUPUT Nullable(Float64),
        DL_THROUPUT Nullable(Float64),
        UL_THROUPUT_MAX Nullable(Float64),
        DL_THROUPUT_MAX Nullable(Float64),
        START_CQI String,
        START_SINR_PUSCH Nullable(Float64),
        START_SINR_PUCCH Nullable(Float64),
        END_CQI String,
        END_SINR_PUSCH Nullable(Float64),
        END_SINR_PUCCH Nullable(Float64),
        ERAB_ID Nullable(String),
        UE_LTE_CATEGORY_UTRA Nullable(Int64),
        MAKE_ID Nullable(Int16),
        MODEL_ID Nullable(Int32),
        FIRST_TA Nullable(Int16),
        LAST_TA Nullable(Int16),
        AVG_TA Nullable(Float64),
        MOVING Nullable(Int8),
        INDOOR Nullable(Int8),
        POS_LAST_LON Nullable(Float64),
        POS_LAST_LAT Nullable(Float64),
        POS_LAST_TILE Nullable(Int64),
        POS_LAST_EUTRABAND Nullable(Int16),
        POS_LAST_EARFCN Nullable(Int32),
        POS_LAST_ENODEB Nullable(Int32),
        POS_LAST_CELL Nullable(Int32),
        POS_LAST_RSRP Nullable(Float64),
        POS_LAST_RSRQ Nullable(Float64),
        POS_LAST_S_EUTRABAND Nullable(Int16),
        POS_LAST_S_EARFCN Nullable(Int32),
        POS_LAST_S_RSRP Nullable(Float64),
        POS_LAST_S_RSRQ Nullable(Float64),
        POS_FIRST_LON Nullable(Float64),
        POS_FIRST_LAT Nullable(Float64),
        POS_FIRST_TILE Nullable(Int64),
        POS_FIRST_EUTRABAND Nullable(Int16),
        POS_FIRST_EARFCN Nullable(Int32),
        POS_FIRST_ENODEB Nullable(Int32),
        POS_FIRST_CELL Nullable(Int32),
        POS_FIRST_RSRP Nullable(Float64),
        POS_FIRST_RSRQ Nullable(Float64),
        POS_FIRST_S_EUTRABAND Nullable(Int16),
        POS_FIRST_S_EARFCN Nullable(Int32),
		POS_FIRST_S_ENODEB Nullable(String),
		POS_FIRST_S_CELL Nullable(String),
		POS_FIRST_S_CELL_NAME Nullable(String),
        POS_FIRST_S_RSRP Nullable(Float64),
        POS_FIRST_S_RSRQ Nullable(Float64),
        POS_FIRST_TILE_LON Nullable(Float64),
        POS_FIRST_TILE_LAT Nullable(Float64),
        POS_LAST_TILE_LON Nullable(Float64),
        POS_LAST_TILE_LAT Nullable(Float64)
)
ENGINE = MergeTree
PRIMARY KEY CALL_ID
ORDER BY CALL_ID

