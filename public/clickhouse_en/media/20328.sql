SELECT DISTINCT
    UMTS.IMSI AS IMSI,
    UMTS.IMEI AS IMEI,
    handset.MANUFACTURER AS MAKER,
    handset.MODEL AS MODEL,
    SUM(LTE_Call_Count) + SUM(UMTS_Call_Count) + SUM(GSM_Call_Count) AS Total_Call_Count,
    SUM(LTE_DROP_CALL) + SUM(UMTS_DROP_CALL) + SUM(GSM_DROP_CALL) AS Total_Drop_Count,
    SUM(LTE_BLOCK_CALL) + SUM(UMTS_BLOCK_CALL) + SUM(GSM_BLOCK_CALL) AS Total_Block_Count,
    SUM(UMTS_PAYLOAD_DL) + SUM(LTE_PAYLOAD_DL) AS TOTAL_PAYLOAD_DL,
    SUM(UMTS_PAYLOAD_UL) + SUM(LTE_PAYLOAD_UL) AS TOTAL_PAYLOAD_UL,
    MAX(THP_4G_DL) AS MAX_Throughput_DL,
    MAX(THP_4G_UL) AS MAX_Throughput_UL,
    AVG(START_RSCP) as START_RSCP,
    AVG(START_ECN0) as START_ECN0,
    AVG(END_RSCP) as END_RSCP,
    AVG(END_ECN0) as END_ECN0,
    AVG(UMTS_CALL_DROP_RATE) as UMTS_CALL_DROP_RATE,
    AVG(UMTS_CSSR) as UMTS_CSSR,
    AVG(UMTS_BLOCK_CALL_RATE) as UMTS_BLOCK_CALL_RATE,
    AVG(START_RSRP) as START_RSRP,
    AVG(START_RSRQ) as START_RSRQ,
    AVG(END_RSRP) as END_RSRP,
    AVG(END_RSRQ) as END_RSRQ,
    AVG(LTE_CALL_DROP_RATE) as LTE_CALL_DROP_RATE,
    AVG(LTE_CSSR) as LTE_CSSR,
    AVG(LTE_BLOCK_CALL_RATE) as LTE_BLOCK_CALL_RATE,
    AVG(START_RXLEVEL) as START_RXLEVEL,
    AVG(START_RXQUAL) as START_RXQUAL,
    AVG(END_RXLEV) AS END_RXLEVEL,
    AVG(GSM_CALL_DROP_RATE) as GSM_CALL_DROP_RATE,
    AVG(GSM_CSSR) as GSM_CSSR,
    AVG(GSM_BLOCK_CALL_RATE) as GSM_BLOCK_CALL_RATE
FROM
(
    SELECT DISTINCT
        IMSI,
        IMEI,
        MAKE_ID,
        MODEL_ID,
        AVG(START_RSRP) AS START_RSRP,
        AVG(START_RSRQ) AS START_RSRQ,
        SUM(COUNT_BLOCK) + SUM(Setup_Failure) AS LTE_BLOCK_CALL,
        SUM(COUNT_DROP) AS LTE_DROP_CALL,
        SUM(Call_Count) AS LTE_Call_Count,
        AVG(END_RSRP) AS END_RSRP,
        AVG(END_RSRQ) AS END_RSRQ,
        (SUM(COUNT_DROP) / SUM(Call_Count)) * 100 AS LTE_CALL_DROP_RATE,
        (1 - ((SUM(COUNT_BLOCK) + SUM(Setup_Failure)) / SUM(Call_Count))) * 100 AS LTE_CSSR,
        ((SUM(COUNT_BLOCK) + SUM(Setup_Failure)) / SUM(Call_Count)) * 100 AS LTE_BLOCK_CALL_RATE,
        max(Max_Throughput_UL) AS THP_4G_UL,
        max(Max_Throughput_DL) AS THP_4G_DL,
        SUM(PAYLOAD_UL) AS LTE_PAYLOAD_UL,
        SUM(PAYLOAD_DL) AS LTE_PAYLOAD_DL
    FROM default.table_tile_lte_tsel_all where toYYYYMMDD(toDate(DATE)) = tanggal
    GROUP BY
        IMSI,
        IMEI,
        MAKE_ID,
        MODEL_ID
) AS LTE
RIGHT JOIN
(
    SELECT DISTINCT
        IMSI,
        IMEI,
        MAKE_ID,
        MODEL_ID,
        AVG(START_RSCP) AS START_RSCP,
        AVG(START_ECN0) AS START_ECN0,
        SUM(COUNT_BLOCK) + SUM(Setup_Failure) AS UMTS_BLOCK_CALL,
        SUM(COUNT_DROP) AS UMTS_DROP_CALL,
        SUM(Call_Count) AS UMTS_Call_Count,
        AVG(END_RSCP) AS END_RSCP,
        AVG(END_ECN0) AS END_ECN0,
        (SUM(COUNT_DROP) / SUM(Call_Count)) * 100 AS UMTS_CALL_DROP_RATE,
        (1 - ((SUM(COUNT_BLOCK) + SUM(Setup_Failure)) / SUM(Call_Count))) * 100 AS UMTS_CSSR,
        ((SUM(COUNT_BLOCK) + SUM(Setup_Failure)) / SUM(Call_Count)) * 100 AS UMTS_BLOCK_CALL_RATE,
        max(MAX_Throughput_UL) AS THP_3G_UL,
        max(MAX_Throughput_DL) AS THP_3G_DL,
        SUM(PAYLOAD_UL) AS UMTS_PAYLOAD_UL,
        SUM(PAYLOAD_DL) AS UMTS_PAYLOAD_DL
    FROM default.table_tile_umts_tsel_all where toYYYYMMDD(toDate(DATE)) = tanggal
    GROUP BY
        IMSI,
        IMEI,
        MAKE_ID,
        MODEL_ID
) AS UMTS ON (LTE.IMSI = UMTS.IMSI) AND (LTE.IMEI = UMTS.IMEI) AND (LTE.MAKE_ID = UMTS.MAKE_ID) AND (LTE.MODEL_ID = UMTS.MODEL_ID)
LEFT JOIN
(
    SELECT DISTINCT
        IMSI,
        IMEI,
        MAKE_ID,
        MODEL_ID,
        AVG(START_RXLEVEL) AS START_RXLEVEL,
        AVG(START_RXQUAL) AS START_RXQUAL,
        SUM(COUNT_BLOCK) + SUM(Setup_Failure) AS GSM_BLOCK_CALL,
        SUM(COUNT_DROP) AS GSM_DROP_CALL,
        SUM(Call_Count) AS GSM_Call_Count,
        AVG(END_RXLEV) AS END_RXLEV,
        AVG(END_RXQUAL) AS END_RXQUAL,
        (SUM(COUNT_DROP) / SUM(Call_Count)) * 100 AS GSM_CALL_DROP_RATE,
        (1 - ((SUM(COUNT_BLOCK) + SUM(Setup_Failure)) / SUM(Call_Count))) * 100 AS GSM_CSSR,
        ((SUM(COUNT_BLOCK) + SUM(Setup_Failure)) / SUM(Call_Count)) * 100 AS GSM_BLOCK_CALL_RATE
    FROM default.table_tile_gsm_tsel_all where toYYYYMMDD(toDate(DATE)) = tanggal
    GROUP BY
        IMSI,
        IMEI,
        MAKE_ID,
        MODEL_ID
) AS GSM ON (UMTS.IMSI = GSM.IMSI) AND (UMTS.IMEI = GSM.IMEI) AND (UMTS.MAKE_ID = GSM.MAKE_ID) AND (UMTS.MODEL_ID = GSM.MODEL_ID)
LEFT JOIN
(
    SELECT DISTINCT
        IMSI,
        MSISDN,
        SIM_TYPE
    FROM default.dim_msisdn_tanggal
) AS MSISDN ON LTE.IMSI = MSISDN.IMSI
LEFT JOIN
(
    SELECT DISTINCT
        toInt64(MAKE_ID) as MAKE_ID,
        MODEL_ID,
        MANUFACTURER,
        MODEL
    FROM default.dim_handset_tanggal
) AS handset ON (LTE.MAKE_ID = handset.MAKE_ID) AND (LTE.MODEL_ID = handset.MODEL_ID)

GROUP BY IMSI,IMEI,MAKER,MODEL
FORMAT CSVWithNames
