SELECT
    DISTINCT
    SITEDB.KABUPATEN as KABUPATEN_CELL,
    KABUPATEN_TILE,
    REGION,
    SITEID,
    SITENAME,
    CELLNAME,
    KPI.TILE_ID,
    AVG(LONGITUDE) as LONGITUDE,
    AVG(LATITUDE) as LATITUDE,
    DominantCells.ENODEBID as ENODEBID,
    TAC,
    DominantCells.CELLID as CELLID,
    DominantCells.BAND as BAND,
    BTS_TYPE,
    AVG(RSRP) as RSRP,
    AVG(RSRQ) as RSRQ,
    SUM(CQI_0) as CQI_0,
    SUM(CQI_1) as CQI_1,
    SUM(CQI_2) as CQI_2,
    SUM(CQI_3) as CQI_3,
    SUM(CQI_4) as CQI_4,
    SUM(CQI_5) as CQI_5,
    SUM(CQI_6) as CQI_6,
    SUM(CQI_7) as CQI_7,
    SUM(CQI_8) as CQI_8,
    SUM(CQI_9) as CQI_9,
    SUM(CQI_10) as CQI_10,
    SUM(CQI_11) as CQI_11,
    SUM(CQI_12) as CQI_12,
    SUM(CQI_13) as CQI_13,
    SUM(CQI_14) as CQI_14,
    SUM(CQI_15) as CQI_15,
    AVG(SINR_PUSCH) as SINR_PUSCH, 
    AVG(SINR_PUCCH) as SINR_PUCCH,
    SUM(COUNT_DROP) AS DROP_CALL_COUNT,
    SUM(COUNT_BLOCK) AS BLOCK_CALL_COUNT,
    SUM(TOTAL_PAYLOAD) as TOTAL_PAYLOAD,
    SUM(PAYLOAD_DL) as PAYLOAD_DL,
    SUM(PAYLOAD_UL) as PAYLOAD_UL,
    AVG(Avg_Throughput_DL) as Avg_Throughput_DL,
    MAX(Max_Throughput_DL) as Max_Throughput_DL,
    AVG(Avg_Throughput_UL) as Avg_Throughput_UL,
    MAX(Max_Throughput_UL) as Max_Throughput_UL,
    SUM(Call_Count) as Call_Count,
    SUM(Subscriber_Count) as Subscriber_Count,
    SUM(Device_Count) as Device_Count
FROM
(
    SELECT DISTINCT
        TILE_ID,
        AVG(LONGITUDE) as LONGITUDE,
        AVG(LATITUDE) as LATITUDE,
        AVG(START_RSRP) AS RSRP,
        AVG(START_RSRQ) AS RSRQ,
        SUM(CQI_0) AS CQI_0,
        SUM(CQI_1) AS CQI_1,
        SUM(CQI_2) AS CQI_2,
        SUM(CQI_3) AS CQI_3,
        SUM(CQI_4) AS CQI_4,
        SUM(CQI_5) AS CQI_5,
        SUM(CQI_6) AS CQI_6,
        SUM(CQI_7) AS CQI_7,
        SUM(CQI_8) AS CQI_8,
        SUM(CQI_9) AS CQI_9,
        SUM(CQI_10) AS CQI_10,
        SUM(CQI_11) AS CQI_11,
        SUM(CQI_12) AS CQI_12,
        SUM(CQI_13) AS CQI_13,
        SUM(CQI_14) AS CQI_14,
        SUM(CQI_15) AS CQI_15,
        AVG(SINR_PUSCH) AS SINR_PUSCH,
        AVG(SINR_PUCCH) AS SINR_PUCCH,
        SUM(COUNT_DROP) AS COUNT_DROP,
        SUM(COUNT_BLOCK) AS COUNT_BLOCK,
        SUM(TOTAL_PAYLOAD) AS TOTAL_PAYLOAD,
        SUM(PAYLOAD_DL) AS PAYLOAD_DL,
        SUM(PAYLOAD_UL) AS PAYLOAD_UL,
        AVG(Avg_Throughput_DL) AS Avg_Throughput_DL,
        MAX(Max_Throughput_DL) AS Max_Throughput_DL,
        AVG(Avg_Throughput_UL) AS Avg_Throughput_UL,
        MAX(Max_Throughput_UL) AS Max_Throughput_UL,
        SUM(Call_Count) AS Call_Count,
        SUM(Subscriber_Count) AS Subscriber_Count,
        SUM(Device_Count) AS Device_Count
    FROM default.table_tile_lte_tanggal 
        GROUP BY
        TILE_ID
        
) AS KPI
LEFT JOIN
(
	SELECT DISTINCT
    DominantCell.TILE_ID as TILE_ID,
	DominantCell.BAND as BAND,
    ENODEBID,
    CELLID,
    CallCount
FROM
(
    SELECT DISTINCT
        TILE_ID,
        ENODEBID,
        CELLID,
		BAND,
        Max(Call_Count) AS CallCount
    FROM default.table_tile_lte_tanggal
    GROUP BY
        TILE_ID,
        ENODEBID,
        CELLID,
		BAND
    HAVING ( ENODEBID > 0 ) AND ( CELLID > 0 )
) AS DominantCell
INNER JOIN
(
    SELECT DISTINCT
        TILE_ID,
        MAX(Call_Count) AS CallCount
    FROM table_tile_lte_tanggal
    GROUP BY TILE_ID
) AS maxcallcount ON (DominantCell.TILE_ID = maxcallcount.TILE_ID) AND (DominantCell.CallCount = maxcallcount.CallCount)
) AS DominantCells on KPI.TILE_ID=DominantCells.TILE_ID
LEFT JOIN
(
    SELECT DISTINCT
        KABUPATEN AS KABUPATEN,
        ENODEBID,
        CELLID,
        REGION,
        SITEID,
        SITENAME,
        CELLNAME,
        TAC,
        SITETYPE AS BTS_TYPE
    FROM default.sitedb_lte_tanggal
) AS SITEDB ON (SITEDB.ENODEBID = DominantCells.ENODEBID) AND (SITEDB.CELLID = DominantCells.CELLID)
LEFT JOIN
(
    SELECT
        DISTINCT
        TILE_ID,
        KAB_ID as KABUPATEN_TILE
    FROM default.nat_tile
) AS NatKab ON KPI.TILE_ID = NatKab.TILE_ID

Group BY
    KABUPATEN_CELL,
    KABUPATEN_TILE,
    REGION,
    SITEID,
    SITENAME,
    CELLNAME,
    KPI.TILE_ID,
    ENODEBID,
    TAC,
    CELLID,
    BAND,
    BTS_TYPE

FORMAT CSVWithNames
