CREATE TABLE table_name3
(
    EventDate DateTime,
    CounterID UInt32,
    UserID UInt32
) ENGINE = ReplicatedMergeTree('/clickhouse/tables/{shard}/table_name3', '{replica}')
PARTITION BY toYYYYMM(EventDate)
ORDER BY (EventDate, intHash32(UserID))
SAMPLE BY intHash32(UserID)



select * from table_name3


insert into table_name3 values('2020-04-04 10:10:11',1,222)

alter table table_name3 delete where 1=1